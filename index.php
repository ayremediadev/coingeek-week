<?php 
	// PHP permanent URL redirection
	header("Location: https://coingeek.com/conferences/", true, 307);
	exit();

	if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<?php
	session_start();
	//print_r($_SESSION);
	session_destroy();//clear past sessions on refresh.
?>
<!DOCTYPE html>
<html lang="en-GB" class="no-js">
	<head>
		<script src="https://coingeekweek.com/jquery-3.3.1.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Coingeek cryptocurrency and blockchain conference | London 28th-30th November 2018</title>
		
		
		<!-- Meta description general -->
		<meta name="robots" 	content="index,follow" />
		<meta name="author" 	content="">
		<meta name="description"content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<meta name="keywords" 	content="CoinGeek Week Conference, Marketing Festival, Marketing event, Marketers, Marketing conference, Brand companies, Marketing consultancy" />
		<meta name="viewport" 	content="width=device-width, initial-scale=1.0" />
		<!-- Meta description -->
		<meta property="og:image" 		content="https://coingeekweek.com/dist/img/social_logo.jpg" />
		<meta property="og:type" 		content="website" />
		<meta property="og:site_name" 	content="Coingeek Conference" />
		<meta property="og:title" 		content="Coingeek cryptocurrency and blockchain conference | London 28th-30th November 2018" />
		<meta property="og:description" content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<meta property="schema:name" typeof="http://schema.org/WebPage" 		content="Coingeek Conference" />
		<meta property="schema:image" typeof="http://schema.org/WebPage" 		content="https://coingeekweek.com/dist/img/social_logo.jpg" />
		<meta property="schema:description" typeof="http://schema.org/WebPage" 	content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<!-- Meta Twitter -->
		<meta name="twitter:card" content="summary_large_image">
		
		<!-- Favicons -->
		<link rel="shortcut icon" href="/dist/img/ico/favicon.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" 	href="/dist/img/ico/144.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" 	href="/dist/img/ico/114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" 		href="/dist/img/ico/72.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" 		href="/dist/img/ico/57.png">

		<link rel="stylesheet" href="dist/css/global.min.css">
		<link rel="stylesheet" href="dist/parsley/parsley.css" type="text/css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

		<!-- Theme CSS -->
		<link href="//fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet">
		<link rel="stylesheet" href="https://use.typekit.net/bea6eml.css">
		
		<!--[if IE]>
			<style>
			.path-rotation::before{
			opacity: 0;
			}
			</style>
		<![endif]-->
		
		<!-- Support for Media Queries in IE8 -->
		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1-1-0/respond.min.js"></script>
		<![endif]-->
		
		<!--\ Google Captcha/-->
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		
		<!-- Google Tag Manager -->
		
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		
		})(window,document,'script','dataLayer','GTM-M9WBCPN');</script>
		
		<!-- End Google Tag Manager -->

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '2100522493292304');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=2100522493292304&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
		<style>
			@media only screen and (min-width: 800px) {
	
				.sponsors.powered.section__body {
					max-width: 90%; 
					margin: auto; 
					padding-bottom: 0;
				}
				.article .article__header .article__header__title.after_party {
					margin-top: 1em; 
				}
				p {
					margin: 0 0 1em 0;
					font-weight: 500 !important;
				}
				.sponsors .panel img.first {
					width: 100% !important;
				}
				.sponsors .panel img {
					filter: none;
					width: 30% !important;
					opacity: 1;
				}
			}
		</style>
		<link rel="stylesheet" type="text/css" href="https://coingeekweek.com/responsive.css"/>
		<!------
		Below script was written to ensure that the ticket order form will not submit unless ALL of the 
		required fields are completed. This is because the "required" attribute may not work across all 
		browsers. 
		=== Chris Brosnan (Tuesday 9th October 2018)
		------->
		<script>
			setInterval(function() {
				var empty = false;
				$('form > input').each(function() {
					// Count initially set at 0, increases by 1 if a required field is completed, reduces
					// by 1 if a required field is blank.Maximum value is 5 (number of required fields), minimum 
					// value is 0. 
					var count = 0;
					<?php // PHP array of all required field names, looped to check values for each in JS script 
					$fields = array('#firstname', '#surname', '#email-1', '#email-2', '#phone'); 
					foreach ($fields as $fi){ ?>
						if ($('<?php echo $fi; ?>').val() == '' ) {
							empty = true;
							if(count < 0){
								count = 0;
							}
							if(count > 0 && count < 5){
								count -= 1;
							}
							$('<?php echo $fi; ?>').siblings().css( "opacity", "1" );
						} else {
							empty = false; 
							count += 1;
							$('<?php echo $fi; ?>').siblings().css( "opacity", "0" );
						}
					<?php } ?>
					if (count < 5) {
						$('#submitForm').attr('disabled', 'disabled');
						$('#submitForm').addClass('disabled'); 
					} else {
						$('#submitForm').removeAttr('disabled');
						$('#submitForm').removeClass('disabled'); 
					}
					//document.getElementById("counter").innerHTML = "The counter is: " + count;
				});
			}, 100)(jQuery);
		</script>
	</head>
	<body class="body--id-9692 t-homepage FOM-2018">
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9WBCPN"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<header class="header">
			<?php include_once 'part/nav-mobile.php'; ?>
			<?php include_once 'part/nav.php'; ?>		
		</header>
		<div class="site">
			<div class="hero">
				<div class="hero__image" style="background: url(dist/img/intro.jpg) no-repeat center / cover;"></div>
				<div class="hero__description">
					<div class="panel panel--default panel--id-9822">
						<div class="panel__header">
							<h4 class="panel__header__title"> CoinGeek Week Conference </h4>
						</div>
						<div class="panel__body">
							<p style="text-align: center;">28th-30th November 2018<br />LONDON</p>
							<p style="text-align: center;"><a class="ck-button-one" href="#buy-pass" target="_self" title="buy-a-pass">BUY TICKET</a></p>
						</div>
					</div>
				</div>
				<div class="hero__intro">
					<div class="section__body" style="padding: 20px;max-width: 1000px;margin: auto;">
						<p style="text-align: justify;font-size: 22px;font-weight: 900;">In November 2018, the true pioneers of cryptocurrency and blockchain will be converging at our CoinGeek Week Conference, allowing them to share their knowledge, experiences and discuss the unique advantages of how cryptocurrency and the blockchain will help your businesses add an extra revenue generating machine. </p>
						<p style="text-align: justify;font-size: 22px;font-weight: 900;">You will become part of this historical event, joining the realms of the pioneers within the Cryptocurrency and Blockchain arena. </p>
						<p style="text-align: center;font-size: 22px;font-weight: 900;"><a class="ck-button-three" href="#buy-pass" target="_self" >Buy Ticket</a></p>
					</div>
					
				</div>
			</div>
			<?php include_once 'part/reveal.php';?>
			<div class="content">
				<main class="content__main">
					<div class="content__main__body">
						
						<?php include_once 'part/headliners.php';?>
						<?php //include_once 'part/speakers.php';?>
						<?php include_once 'part/sponsors.php';?>
						<?php include_once 'part/afterParty.php';?>
						<?php include_once 'part/testimonials.php';?>
						
					</div>
				</main>
			</div>
			<?php include_once 'part/agenda.php';?>

			<?php include_once 'part/minersday.php';?>
			
			<?php include_once 'part/tickets.php';?>
			
			<?php include_once 'part/form-rocketr.php';?>
			
			<?php include_once 'part/moreinfo.php';?>
		
			<?php include_once 'part/footer.php'; ?>
		</div>
		<!-- JS -->
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="dist/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="dist/js/script.min.js"></script>
		
		<script src="dist/js/showoff.global.js" ></script>
		
		<script src="/dist/js/easytimer.min.js"></script>
		<!-- Swiper JS -->
		<script src="/dist/js/swiper.min.js"></script>
		<!-- Custom JS -->
		<script src="/dist/js/custom.js"></script>
		<!-- Parserly JS -->
		<script type="text/javascript" src="dist/parsley/parsley.js"></script>
		<script type="text/javascript" src="dist/parsley/parsley_custom.js"></script>
		<!-- IE CSS FIX -->
		<script>
			var isIE = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
			if(isIE){
				//document.getElementsByClassName("path-rotation").style.opacity = "0";
				//var x = document.getElementsByClassName("path-rotation");
				//x[0].style.opacity = "0";
				$(".path-rotation").attr('style',  'opacity:0!important');
				//alert('internet explorer');
			}
		</script>
		<!-- Start of LiveChat (www.livechatinc.com) code -->
		<script type="text/javascript">
			window.__lc = window.__lc || {};
			window.__lc.license = 9956175;
			(function() {
				var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
				lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
			})();
		</script>
		<!-- End of LiveChat code -->
	</body>
</html>									