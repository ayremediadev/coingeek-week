<!-- Demo styles -->
<div id="agenda" class="section section--one-column section--id-5 section--feature">
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<a class="anchor"></a> 
						<article class="article article--featured">
				<div class="article__image"> 
					<img src="/dist/img/general/Agenda2.jpg" /> 
				</div>
				<div class="agenda_cont">
					<!-- <div class="article__header">
						<h3 class="article__header__title"> Agenda </h3>
						<p style="color:#ffffff; margin:auto; max-width:880px; "> Enjoy three days filled with expert talks, Q&As and networking. Click below to discover the timelines. Full agenda, including speakers, will be revealed soon. </p>
						<i id="backToAgenda"></i>
					</div> -->

						<div class="accordion">
							<div class="tab">
								<input id="tab-one" type="checkbox" name="tabs">
								<label for="tab-one">28th November 2018</label>
								<div class="tab-content">
									<br>
									<h3 style="color: #000;font-size: 22px;">Application Developers Day</h3>
									<p style="color: #000; font-size: 14px;" >On Wednesday you will learn everything you need to know about the latest within blockchain technology. Learn from the industry pioneers and immerse yourself into the great opportunities blockchain technology will bring to your business. We welcome all our attendees to keep the exciting conversation going at the afternoon networking event.  
									</p>
									<table class="stack desktopAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td colspan="2">REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td>Introduction to CoinGeek Week</td>
												<td>Calvin Ayre (Ayre Media)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td><span color="#ff0000;">FREE SPACE</span></td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>The Future of Commerce</td>
												<td>James Belding (Tokenized)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>BCH & a Global Supply Chain solution in the world's largest ERP system</td>
												<td>Stephan Nilsson (Unisot)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Money Button: Easy Payments for Everybody Everywhere</td>
												<td>Ryan X. Charles</td>  
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Taking Care of Business:  Business Uses of Bitcoin</td>
												<td>Dr. Craig S. Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td colspan="2">Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Smart Contracts, Dumb Contracts - How To Build a Business on the Blockchain</td>
												<td>Kristy-Leigh Minehan</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>The New Money</td>
												<td>Alex Agut (HandCash)</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Human Readable Data in the Block Chain</td>
												<td>Erich Erstu (CryptoGraffiti.info)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30pm-3:45pm</td>
												<td>Building B2B Solutions on Bitcoin for Industry 4.0</td>
												<td>Rogelio Reyna (Binde)</td>  
											</tr>
											<tr>
												<td style="width:150px;">3:45-4:15pm</td>
												<td>Panel - What’s Next for building on Bitcoin SV Alex Agut, Handcash Jerry Chan, SBI Ryan X Charles, Money Button</td>
												<td>Jimmy Nguyen</td>
											</tr>
											<tr>
												<td style="width:150px;">4:15-4:30pm</td>
												<td>Closing remarks</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
										</tbody>
									</table>
									<table class="stack mobileAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td>REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td>Introduction to CoinGeek Week, Calvin Ayre (Ayre Media)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td><span color="#ff0000;">FREE SPACE</span>, Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>The Future of Commerce, James Belding (Tokenized)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>BCH & a Global Supply Chain solution in the world's largest ERP system, Stephan Nilsson (Unisot)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Money Button: Easy Payments for Everybody Everywhere, Ryan X. Charles</td>  
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Taking Care of Business:  Business Uses of Bitcoin, Dr. Craig S. Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td>Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Smart Contracts, Dumb Contracts - How To Build a Business on the Blockchain, Kristy-Leigh Minehan</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>The New Money, Alex Agut (HandCash)</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Human Readable Data in the Block Chain, Erich Erstu (CryptoGraffiti.info)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30pm-3:45pm</td>
												<td>Building B2B Solutions on Bitcoin for Industry 4.0, Rogelio Reyna</td>  
											</tr>
											<tr>
												<td style="width:150px;">3:45-4:15pm</td>
												<td>Panel - What’s Next for building on Bitcoin SV Alex Agut, Handcash Jerry Chan, SBI Ryan X Charles, Money Button, Jimmy Nguyen</td>
											</tr>
											<tr>
												<td style="width:150px;">4:15-4:30pm</td>
												<td>Closing remarks, Jimmy Nguyen (nChain)</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab">
								<input id="tab-two" type="checkbox" name="tabs">
								<label for="tab-two">29th November 2018</label>
								<div class="tab-content">
									<br>
									<h3 style="color: #000;font-size: 22px;">Application Merchants Day</h3>
									<p style="color: #000; font-size: 14px;" >Tune in on Thursday to get the advantage over your competitors. The leaders of the industry will enlighten you on new developments, updates and demonstrate how adopting Bitcoin Cash is the fundament for growing your business. We welcome all our attendees to keep the exciting conversation going at the afternoon networking event.
									</p>
									<table class="stack desktopAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td colspan="2">REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td></td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>Why to be passionate about Bitcoin?</td>
												<td>Daniel Lipshitz (GAP600)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Web 3.0 on Bitcoin</td>
												<td>Jason Chavannes (Memo.bch)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Beer + People = Unicorns</td>
												<td>Martin Dempster (Brewdog)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Building a Two-Sided Network</td>
												<td>Elizabeth White (The White Company)</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>The future of work, the future of tax and the future of money</td>
												<td>Dominic Frisby (Moneyweek)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td colspan="2">Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Merchant Payments and a Better User Experience with Bitcoin</td>
												<td>Lorien Gamaroff (Centbee)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Cryptocurrency Adoption for Payments</td>
												<td><span color="#ff0000;">Ina Samovich</span></td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Designing a Better World</td>
												<td>Widya Salim (Cryptartica)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:30pm</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:00pm</td>
												<td> Instant transactions for Bitcoin</td>
												<td>Steve Shadders (nChain)</td>  
											</tr>
											<tr>
												<td style="width:150px;">4:00-4:30pm</td>
												<td>Bitcoin payments in the real world</td>
												<td>Angus Brown (CentBee)</td>
											</tr>
											<tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Closing Remarks</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">4:45pm</td>
												<td colspan="2">Cocktail Reception</td>
											</tr>
										</tbody>
									</table>
									<table class="stack mobileAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td>REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>Why to be passionate about Bitcoin?, Daniel Lipshitz (GAP600)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Web 3.0 on Bitcoin, Jason Chavannes (Memo.bch)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Beer + People = Unicorns, Martin Dempster (Brewdog)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Building a Two-Sided Network, Elizabeth White (The White Company)</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>The future of work, the future of tax and the future of money, Dominic Frisby (Moneyweek)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td>Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Merchant Payments and a Better User Experience with Bitcoin, Lorien Gamaroff (Centbee)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Cryptocurrency Adoption for Payments, Ina Samovich</span></td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Designing a Better World, Widya Salim (Cryptartica)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:30pm</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:30pm</td>
												<td>Instant transactions for Bitcoin, Steve Shadders (nChain)</td>  
											</tr>
											<tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Bitcoin payments in the real world, Angus Brown (CentBee)</td>
											</tr>
											<tr>
												<td style="width:150px;">4:45-5:15pm</td>
												<td>Closing Remarks, Jimmy Nguyen (nChain)</td>
											</tr>
											<!-- <tr>
												<td style="width:150px;">5:15-5:30pm</td>
												<td>Closing Remarks, Jimmy Nguyen (nChain)</td>
											</tr> -->
										</tbody>
									</table>									
								</div>
							</div>
							<div class="tab">
								<input id="tab-three" type="checkbox" name="tabs">
								<label for="tab-three">30th November 2018</label>
								<div class="tab-content">
									<br><h3 style="color: #000;font-size: 22px;">The Future</h3>
									<p style="color: #000; font-size: 14px;" >Friday will round off the week with keynotes from the true pioneers of blockchain. Add to everything you have learned this week by taking in what our experts are predicting in the future of Bitcoin Cash technology. After an intense week of learning, it’ll be time to relax, we invite all our ‘all-days’ ticket holders to an extravagant celebration at and exclusive venue in central London, which will be in a true Calvin Ayre style. 
									</p>
									<table class="stack desktopAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td colspan="2">REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9:10-9:30am</td>
												<td></td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>To the Moon!</td>
												<td>Jerry Chan (SBI Holdings)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Bringing the Crypto Space out of the Basement and into the Boardroom</td>
												<td>Taras Kulyk</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Dominic Frisby's Big Money Gameshow</td>
												<td>Dominic Frisby (MoneyWeek)</td>
											</tr>
											<tr>
												<td style="width:150px;">11:00-11:20am</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>In their Own Words:  Bitcoin Merchant Testimonials – Panel discussion<br/>
													Moderated by: Jimmy Nguyen<br/>BrewDog – Martin Dempster, VP of Innovation<br/>White Company – Elizabeth White, CEO<br/>Coppay - Ina Samovich, CEO<br/>CentBee – Heidi Patmore, Head of Marketing
												</td>
												<td>TBC</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Going Meta on Bitcoin</td>
												<td>Dr. Craig Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td colspan="2">Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30-2:00pm</td>
												<td>Bitcoin: Global Adoption Starts Here</td>
												<td>Kristy-Leigh Minehan (Core Scientific)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Tools of Humanity</td>
												<td>Michael Hudson, Bitstocks</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>What Role Should Exchanges Play in Bitcoin Governance? MD, Circle. Former CSO, OKEx & OKCoin</td>
												<td>Jack C. Liu (Circle)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:30pm</td>
												<td>Growing Up: It's Time for Bitcoin and Bcommerce to Professionalize<br/>
												Moderated by: Jimmy Nguyen<br />
												Edgar Maximillian <br /> 
												Iqbal Gandham  - eToro<br />Michael Hudson
											</td>
												<td>TBC</td>  
											</tr>
											<!-- <tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Closing remarks</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:00pm-till late</td>
												<td colspan="2">Party</td>
											</tr> -->
										</tbody>
									</table>
									<table class="stack mobileAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td>REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9:10-9:30am</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>To the Moon!, Jerry Chan (SBI Holdings)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Bringing the Crypto Space out of the Basement and into the Boardroom, Taras Kulyk</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Dominic Frisby's Big Money Gameshow, Dominic Frisby (MoneyWeek)</td>
											</tr>
											<tr>
												<td style="width:150px;">11:00-11:20am</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>In their Own Words:  BCH Merchant Testimonials – Panel discussion<br/>
													Moderated by: Jimmy Nguyen<br/>
													BrewDog – Martin Dempster, VP of Innovation<br />
													White Company – Elizabeth White, CEO<br/>
													Coppay - Ina Samovich, CEO<br />CentBee – Heidi Patmore, Head of Marketing (TBC)</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Going Meta on Bitcoin, Dr. Craig Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td>Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30-2:00pm</td>
												<td>Bitcoin: Global Adoption Starts Here, Kristy-Leigh Minehan (Core Scientific)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Tools of Humanity, Michael Hudson (Bitstocks)</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>What Role Should Exchanges Play in Bitcoin Governance? MD, Circle. Former CSO, OKEx & OKCoin, Jack C. Liu (Circle)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:30pm</td>
												<td>Growing Up: It's Time for Bitcoin and Bcommerce to Professionalize<br/>
												Moderated by: Jimmy Nguyen<br/>
												Edgar Maximillian<br/>
												Iqbal Gandham  - eToro<br/>
											Michael Hudson, TBC</td>  
											</tr>
											<!-- <tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Closing remarks, Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:00pm-till late</td>
												<td>Party</td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</div>
						</div>
				
						
						
						</div>
					</article>
				</div>
			</div>
		</div>												