<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <div id="buy-pass" class="cta">
      <div class="panel panel--default panel--id-9857">
        <div class="panel__header">
          <h4 class="panel__header__title"> CoinGeek Week Conference Tickets </h4>
        </div>
        <div class="panel__body">
          <p style="text-align: center;">28th-30th November 2018</p>
        </div>
        <div class="body-pass">
          <p style="text-align: center">For debit/credit card payment through
            Eventbrite, please <a title="Coin Geek Week on Eventbrite"

              target="_blank" href="https://www.eventbrite.co.uk/e/coingeek-week-cryptocurrency-and-blockchain-conference-tickets-49939393069">click
              here</a>.</p>
          <div style="color:#ffffff; margin:auto; max-width:600px; "> <a title="Coin Geek Week on Eventbrite"

              target="_blank" href="https://www.eventbrite.co.uk/e/coingeek-week-cryptocurrency-and-blockchain-conference-tickets-49939393069"><img

                alt="Eventbrite accepts Mastercard, VISA, and American Express"

                src="/dist/img/rocketr/debit_credit_cards.png"></a> </div>
        </div>
        <!--<div id="form-rocketr">
          <div class="bt-form__wrapper">
            <form id="form-step-1" class="uk-form bt-flabels js-flabels required"

              name="contact-form" action="rocketr/contactForm.php" method="POST"

              enctype="multipart/form-data" data-parsley-validate="" data-parsley-errors-messages-disabled="">
              <input id="tierType" name="tierType" value="tier1" type="hidden">
              <div class="half_wrapper">
                <div class="bt-flabels__wrapper"> <input class="form-control required"

                    name="firstname" id="firstname" placeholder="First Name*" autocomplete="off"

                    data-parsley-required="" required="" type="text"> <span class="bt-flabels__error-desc">Required</span>
                </div>
                <div class="bt-flabels__wrapper"> <input class="form-control required"

                    name="surname" id="surname" placeholder="Last Name*" autocomplete="off"

                    data-parsley-required="" required="" type="text"> <span class="bt-flabels__error-desc">Required</span>
                </div>
              </div>
              <div class="bt-flabels__wrapper" style="width: 99%;margin: 15px auto;">
                <input id="email-1" class="form-control required" name="email" placeholder="Your Email*"

                  data-parsley-required="" data-parsley-type="email" data-parsley-equalto="#email-2"

                  autocomplete="off" required="" type="text"> <span class="bt-flabels__error-desc">Required/Invalid
                  Email</span> </div>
              <div class="bt-flabels__wrapper" style="width: 99%;margin: 15px auto;">
                <input id="email-2" class="form-control" name="confirmEmail" placeholder="Confirm Email*"

                  data-parsley-required="" data-parsley-type="email" data-parsley-equalto="#email-1"

                  autocomplete="off" required="" type="text"> <span class="bt-flabels__error-desc">Email
                  Not Matching</span> </div>
              <div class="bt-flabels__wrapper" style="width: 99%;margin: 15px auto;">
                <input class="form-control" name="company" placeholder="Company Name"

                  autocomplete="off" type="text"> </div>
              <div class="bt-flabels__wrapper" style="width: 99%;margin: 15px auto;">
                <input class="form-control" name="industry" placeholder="Industry"

                  autocomplete="off" type="text"> </div>
              <div class="bt-flabels__wrapper" style="width: 99%;margin: 15px auto;">
                <input class="form-control required" name="phone" id="phone" placeholder="Your Phone*"

                  minlength="8" data-parsley-required="" autocomplete="off" required=""

                  type="text"> <span class="bt-flabels__error-desc">Required/Invalid
                  Phone</span> </div>
              <div class="checkx">
                <div class="input_wrapper"> <br>
                  <b>Industry:</b> <br>
                  <div class="half_wrapper">
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="merchant" id="1_1" value="Merchant" type="checkbox">
                      <label class="el-checkbox-style" for="1_1"></label>
                      <p style="display:inline-block;">Merchant</p>
                    </div>
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="developer" id="1_2" value="Bitcoin Apps Developer"

                        type="checkbox"> <label class="el-checkbox-style" for="1_2"></label>
                      <p style="display:inline-block;">Bitcoin Apps Developer</p>
                    </div>
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="miner" id="1_3" value="Miner" type="checkbox">
                      <label class="el-checkbox-style" for="1_3"></label>
                      <p style="display:inline-block;">Miner</p>
                    </div>
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="blockchain" id="1_4" value="Blockchain Developer"

                        type="checkbox"> <label class="el-checkbox-style" for="1_4"></label>
                      <p style="display:inline-block;">Blockchain Developer</p>
                    </div>
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="exchange" id="1_5" value="Hedge Fund / Exchange"

                        type="checkbox"> <label class="el-checkbox-style" for="1_5"></label>
                      <p style="display:inline-block;">Hedge Fund / Exchange</p>
                    </div>
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="venture" id="1_6" value="Venture Capitalist"

                        type="checkbox"> <label class="el-checkbox-style" for="1_6"></label>
                      <p style="display:inline-block;">Venture Capitalist</p>
                    </div>
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="media" id="1_7" value="Media" type="checkbox">
                      <label class="el-checkbox-style" for="1_7"></label>
                      <p style="display:inline-block;">Media</p>
                    </div>
                    <div class="el-checkbox el-checkbox-green"> <span class="margin-r"></span>
                      <input name="investor" id="1_8" value="Investor" type="checkbox">
                      <label class="el-checkbox-style" for="1_8"></label>
                      <p style="display:inline-block;">Investor</p>
                    </div>
                  </div>
                </div>
                <div class="input_wrapper"> <br>
                  <label class="el-switch el-switch-green"> <input class="" name="subscribe"

                      checked="checked" type="checkbox"> <span class="el-switch-style"></span>
                  </label>
                  <p class="el-switch-p">I want to receive updates</p>
                </div>
              </div>
              <br>
              <div id="counter"></div>
              <div class="recaptcha-cont">
                <div class="g-recaptcha" data-sitekey="6LfajGQUAAAAAOq1_OaygmMCSVDedFYXZesLrO3m"></div>
              </div>
              <br>
              <div class="uk-text-center uk-margin-top"> <span class="input-group-btn align-center"><button

                    href="" type="submit" id="submitForm" class="btn btn-form btn-black display-4">Next</button></span>
              </div>
              <br>
            </form>
          </div>
        </div>-->
      </div>
    </div>
  </body>
</html>
