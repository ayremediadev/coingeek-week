<div id="mySidenav" class="sidenav">
	<!-- Mobile Menu-->
	<nav class="nav" role="navigation">
		<ul class="nav__list">
			<li>
				<p style="text-align: center;"><a class="ck-button-one" href="#headliners" target="_self" >Headliners</a></p>
			</li>
			<!--
			<li>
				<p style="text-align: center;"><a class="ck-button-one" href="#speakers" target="_self" >Speakers</a></p>
			</li>
			-->
			<li>
				<p style="text-align: center;"><a class="ck-button-one" href="/speakers/" target="_self" >Speakers</a></p>
			</li>
			<li>
				<p style="text-align: center;"><a class="ck-button-one" href="#sponsors" target="_self" >Sponsors</a></p>
			</li>
			<li>
				<p style="text-align: center;"><a class="ck-button-one" href="#agenda" target="_self" >AGENDA</a></p>
			</li>
			<li>
				<p style="text-align: center;"><a class="ck-button-one" href="#moreinfo" target="_self" >MORE INFO</a></p>
			</li>
			<li>
				<p style="text-align: center;"><a class="ck-button-one" href="#buy-pass" target="_self" >BUY TICKET</a></p>
			</li>
			<li>				
				<div class="ck-social-icons"> 
					<a class="fa fa-youtube" href="https://www.youtube.com/channel/UC95_Nqes9m5arhoT1lt1SFg" target="_blank" title="https://www.youtube.com/channel/UC95_Nqes9m5arhoT1lt1SFg"></a>
					<a class="fa fa-twitter" href="https://twitter.com/realcoingeek" target="_blank" title="https://twitter.com/realcoingeek"></a>
					<a class="fa fa-facebook" href="https://www.facebook.com/realcoingeek/" target="_blank" title="https://www.facebook.com/realcoingeek/"></a>
				</div>
			</li>
			<!-- \ This is the full menu Style in case of submenu /--/>
				<li>
				<input id="group-2" type="checkbox" hidden />
				<label for="group-2"><span class="fa fa-angle-right"></span> First level</label>
				<ul class="group-list">
				<li>
				<li><a href="#">1st level item</a></li>
				<li><a href="#">1st level item</a></li>
				<input id="sub-group-2" type="checkbox" hidden />
				<label for="sub-group-2"><span class="fa fa-angle-right"></span> Second level</label>
				<ul class="sub-group-list">
				<li><a href="#">2nd level nav item</a></li>
				<li><a href="#">2nd level nav item</a></li>
				<li>
				<input id="sub-sub-group-2" type="checkbox" hidden />
				<label for="sub-sub-group-2"><span class="fa fa-angle-right"></span> Third level</label>
				<ul class="sub-sub-group-list">
				<li><a href="#">3rd level nav item</a></li>
				</ul>
				</li>
				</ul>
				</li>
				</ul>
				</li>
			-->
		</ul>
	</nav>
	
</div>