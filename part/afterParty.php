<!-- Demo styles -->
<style>
	.after_cont {
    margin: auto;
    text-align: center;
    width: 100%;
	}
	.after_cont h3 {
    color: #fff;
    font-weight: 900 !important;
    font-size: 50px;
	}
	
</style>
<div id="afterParty" class="section section--one-column section--id-5 section--feature">
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<article class="article article--featured">
				<div class="article__image"> 
					<img src="/dist/img/general/after_party.jpg" /> 
				</div>
				<div class="after_cont">
					<div class="article__header">
						<h3 class="article__header__title after_party"> After-Party </h3>
						<p> After three days of learning and debating on blockchain technology and how Bitcoin Cash is turning eCommerce into bCommerce, you deserve to let your hair down and party in true Calvin Ayre style. Calvin’s parties have earned a well-deserved reputation as truly unmissable events, and the CoinGeek bash promises to be the Party of 2018. To be eligible to attend this party of all parties, you have to attend the full conference.</p>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>
		