<?php $homeURL = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; 
//echo $homeURL; ?>

<div id="speakers" class="section section--one-column section--id-3 section--speakers">
	<div class="section__header">
		<h2 class="section__header__title"> 2018 Speakers </h2>
	</div>
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<article class="article article--default">
				<div class="article__body">
					<div class="js-librarylistwrapper" data-totalcount="17" data-librarytitle="Speakers">
						<div class="m-speakers-list m-speakers-list--grid-custom">
							<div class="m-speakers-list__search"> 
							</div>
							<div class="m-speakers-list__loading">
								<div class="p-loader p-loader--overlay js-w-library-loading" searchgroup="10062B6D-speakers-library"> 
									<span class="p-loader__icon"></span> 
									<span class="p-loader__text">Loading</span> 
								</div>
							</div>
							<div class="m-speakers-list__summary">
								<div class="js-library-summary" searchgroup="10062B6D-speakers-library" data-cachestatic="0"></div>
							</div>
							<ul class="m-speakers-list__items js-library-list" searchgroup="10062B6D-speakers-library" searchrefresh=".js-m-speakers-list-count">
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12831" data-title="Ryan Charles" data-content-type="Speaker" data-image="/dist/img/speakers/ryan-charles.jpg" data-company-name="money button" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Founder and CEO" data-slug="speakers-library/ryan-charles">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('<?php echo $homeURL; ?>dist/img/speakers/ryan-charles.jpg');"> <img src="/dist/img/speakers/ryan-charles.jpg" alt="Ryan Charles" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/ryan-charles','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/alessandra-di-lorenzo&quot;});"> Ryan Charles </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Founder and CEO, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Money Button</span> 
											</div>
										</div>
									</div>
								</li>
								
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12835" data-title="Dan Reed" data-content-type="Speaker" data-image="__resource/peopleProfiles/FE5C39A-72DC-46A7-B948-84D269CFA905-profileimgupload.jpg" data-company-name="Barclaycard Business" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Head of Digital Capabilities" data-slug="speakers-library/dan-reed">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/FE5C39A-72DC-46A7-B948-84D269CFA905-profileimgupload.jpg');"> <img src="__resource/peopleProfiles/FE5C39A-72DC-46A7-B948-84D269CFA905-profileimgupload.jpg" alt="Dan Reed" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/dan-reed','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/dan-reed&quot;});"> Dan Reed </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Head of Digital Capabilities, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Barclaycard Business</span> 
											</div>
										</div>
									</div>
								</li>
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12836" data-title="Daniel Fellows" data-content-type="Speaker" data-image="__resource/peopleProfiles/2CF8320-560A-47B5-932D-A38B6EB6F80A-profileimgupload.jpg" data-company-name="Indeed" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Evangelist" data-slug="speakers-library/daniel-fellows">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/2CF8320-560A-47B5-932D-A38B6EB6F80A-profileimgupload.jpg');"> <img src="__resource/peopleProfiles/2CF8320-560A-47B5-932D-A38B6EB6F80A-profileimgupload.jpg" alt="Daniel Fellows" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/daniel-fellows','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/daniel-fellows&quot;});"> Daniel Fellows </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Evangelist, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Indeed</span> 
											</div>
										</div>
									</div>
								</li>
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12837" data-title="Elizabeth Uviebinene" data-content-type="Speaker" data-image="__resource/peopleProfiles/89B30C7-8426-4455-AB61-FF78B6E3038C-profileimgupload.jpg" data-company-name="Slay in Your Lane" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Co-Author" data-slug="speakers-library/elizabeth-uviebinene">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/89B30C7-8426-4455-AB61-FF78B6E3038C-profileimgupload.jpg');"> <img src="__resource/peopleProfiles/89B30C7-8426-4455-AB61-FF78B6E3038C-profileimgupload.jpg" alt="Elizabeth Uviebinene" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/elizabeth-uviebinene','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/elizabeth-uviebinene&quot;});"> Elizabeth Uviebinene </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Co-Author, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Slay in Your Lane</span> 
											</div>
										</div>
									</div>
								</li>
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12838" data-title="Georgios Chiotis" data-content-type="Speaker" data-image="__resource/peopleProfiles/E52B74D-18D4-41A8-B90F-EA60DC0DC6B2-profileimgupload.jpg" data-company-name="Generator Hostels" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Marketing Director" data-slug="speakers-library/georgios-chiotis">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/E52B74D-18D4-41A8-B90F-EA60DC0DC6B2-profileimgupload.jpg');"> <img src="__resource/peopleProfiles/E52B74D-18D4-41A8-B90F-EA60DC0DC6B2-profileimgupload.jpg" alt="Georgios Chiotis" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/georgios-chiotis','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/georgios-chiotis&quot;});"> Georgios Chiotis </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Marketing Director, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Generator Hostels</span> 
											</div>
										</div>
									</div>
								</li>
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12840" data-title="Hoa Ly" data-content-type="Speaker" data-image="__resource/peopleProfiles/77F48BA-3794-4E80-A194-F06B5737D6C8-profileimgupload.jpg" data-company-name="Shim" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="CEO, Co-Founder" data-slug="speakers-library/hoa-ly">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/77F48BA-3794-4E80-A194-F06B5737D6C8-profileimgupload.jpg');"> <img src="__resource/peopleProfiles/77F48BA-3794-4E80-A194-F06B5737D6C8-profileimgupload.jpg" alt="Hoa Ly" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/hoa-ly','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/hoa-ly&quot;});"> Hoa Ly </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> CEO, Co-Founder, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Shim</span> 
											</div>
										</div>
									</div>
								</li>
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12841" data-title="Jon Davies" data-content-type="Speaker" data-image="__resource/peopleProfiles/77026E4-11A4-4C54-90CC-145C1505F1D5-profileimgupload.jpg" data-company-name="Vodafone" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Head of Digital" data-slug="speakers-library/jon-davies">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/77026E4-11A4-4C54-90CC-145C1505F1D5-profileimgupload.jpg');"> <img src="__resource/peopleProfiles/77026E4-11A4-4C54-90CC-145C1505F1D5-profileimgupload.jpg" alt="Jon Davies" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/jon-davies','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/jon-davies&quot;});"> Jon Davies </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Head of Digital, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Vodafone</span> 
											</div>
										</div>
									</div>
								</li>
								
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12844" data-title="Lindsey Fish" data-content-type="Speaker" data-image="__resource/peopleProfiles/7346BD1-E560-4834-829A-AAEA11B8B90F-profileimgupload.jpg" data-company-name="Mums Enterprise" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Founder and CEO" data-slug="speakers-library/lindsey-fish">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/7346BD1-E560-4834-829A-AAEA11B8B90F-profileimgupload.jpg');"> <img src="__resource/peopleProfiles/7346BD1-E560-4834-829A-AAEA11B8B90F-profileimgupload.jpg" alt="Lindsey Fish" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/lindsey-fish','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/lindsey-fish&quot;});"> Lindsey Fish </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Founder and CEO, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">Mums Enterprise</span> 
											</div>
										</div>
									</div>
								</li>
								
								<li class="m-speakers-list__items__item js-library-item" data-content-i-d="12848" data-title="Martine Van Der Lee" data-content-type="Speaker" data-image="__resource/peopleProfiles/6C4092D-1614-434C-B3EF-45937B60BB23-profileimgupload.png" data-company-name="KLM Royal Dutch Airlines" data-library-type="speaker" data-library-name="Speakers" data-library-i-d="9691" data-job-title="Director of Social Media" data-slug="speakers-library/martine-van-der-lee">
									<a href="" class="m-speakers-list__items__item__image js-librarylink-entry" style="background-image: url('__resource/peopleProfiles/6C4092D-1614-434C-B3EF-45937B60BB23-profileimgupload.png');"> <img src="__resource/peopleProfiles/6C4092D-1614-434C-B3EF-45937B60BB23-profileimgupload.png" alt="Martine Van Der Lee" /> </a> 
									<div class="m-speakers-list__items__item__header-body">
										<div class="m-speakers-list__items__item__header">
											<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry" href="javascript:openRemoteModal('https://testayremedia.ga/conference/speakers-library/martine-van-der-lee','ajax',{},false,'',{&quot;dimension15&quot;:&quot;speakers-library/martine-van-der-lee&quot;});"> Martine Van Der Lee </a> </h3>
											<div class="m-speakers-list__items__item__header__meta"> 
												<span class="m-speakers-list__items__item__header__meta__position"> Director of Social Media, </span> 
												<span class="m-speakers-list__items__item__header__meta__company">KLM Royal Dutch Airlines</span> 
											</div>
										</div>
									</div>
								</li>
							</ul>
							<div class="m-speakers-list__pagination m-speakers-list__pagination--bottom">
								<div class="pagination pagination--default js-pagination js-library-pagination" searchgroup="10062B6D-speakers-library" data-searchcriteria="categories=0EFEBC09-5056-B731-4C8EB6BC0B001197,FDAC13D1-5056-B733-83CD3F5E88ABDDE4" data-cachestatic="0"> 
								</div>
							</div>
						</div>
					</div>
					<p></p>
					<!--<p style="text-align: right;"><a class="ck-button-three" href="2018-speakers" target="_self" title="2018-speakers">VIEW ALL 2018&nbsp;SPEAKERS</a></p>-->
				</div>
			</article>
		</div>
	</div>
</div>