<div id="minersday" class="cta">
	<div class="panel panel--default panel--id-9857">
		<div class="panel__header">
			<h2 class="panel__header__title" style="color: #fff; font-size: 50px;">
				<p style="text-align: center;">Miner’s Day - 27 November 2018</p>
			</h2>
		</div>
		<div class="panel__body">
			<p style="text-align: center;">(Invitation Only)</p>		
		</div>
		<div class="section__body" style="padding: 20px;max-width: 1000px;margin: auto; font-weight: 900; color: #fff; padding-bottom: 90px;">
			<p style="text-align: center;">Miners are a critical part of the BCH ecosystem and the future world of bCommerce. Before the official CoinGeek Week events begin, we have a special day dedicated to Bitcoin mining!</p>
			<p style="text-align: center">This is an invitation-only day for miners, mining pool representatives, equipment manufacturers, mining software developers, and others involved in Bitcoin mining. To request a spot, please complete this form and tell us your role in the mining ecosystem. Space is limited!</p>
			<p style="text-align: center;">
				<a class="ck-button-one" href="/minersday" style="color: #fff; font-size: 16px;" target="_blank" title="find-out-more">Find out more</a>
			</p>
		</div>
		

	</div>
</div>