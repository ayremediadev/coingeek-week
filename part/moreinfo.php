<div id="moreinfo" class="section section--one-column section--id-4 section--sponsors" style="position: relative; text-align: center; padding-top: 0; background: #e6ecef; color: #000;">
	<div class="section__header">
        <h2 class="section__header__title" style="color: #000;">
            <br/>
			<!--<p style="text-align: center;">Would you like to <br>  sponsor CoinGeek Week?</p>-->
			<p style="text-align: center;">More information</p>
		</h2>
	</div>

	<div class="section__body" style="padding: 20px;max-width: 1000px;margin: auto; font-weight: 900; color: #000;">
        <p><strong>A great venue for a great time in London</strong></p>
        <p>CoinGeek has secured the Mermaid Theatre for its conference. It’s a prime venue just yards from the Thames and across the river from the reconstructed Globe Theatre.</p>
        <p>The Mermaid was famous for its annual Christmas production of Treasure Island. The ghost of Long John Silver, with his wooden leg and parrot on his shoulder, could easily be revived by talk of coins and money-making!</p>
        <p>There’s a wide choice of restaurants within easy reach of the Mermaid, from distinctive South African cuisine and river views at <a href="http://www.hightimber.com/" target="_blank">High Timber</a> (7 minute walk) to the award-winning delicacies of the <a href="https://www.chinesecricketclub.com/" target="_blank">Chinese Cricket Club</a> ( 4 minutes) or if you want a traditional London pub, there’s a seasonal menu and cask ales at the handy <a href="https://www.shawsbooksellers.co.uk/" target="_blank">Shaw’s Booksellers</a> (2 minutes).</p>
        <p>Why not fit in a little sight-seeing during a conference break? <a href="https://www.stpauls.co.uk/" target="_blank">St Paul’s Cathedral</a> is only a 5 minute walk. And <a href="https://www.hrp.org.uk/tower-of-london/#gs.HZW8RZI" target="_blank">The Tower of London</a> is 15 minutes, to Tower Hill station, just three stops away on the tube.</p>
        <p>If you need somewhere to stay, there’s a choice of good hotels. CoinGeek has negotiated a 15 per cent discount at the slick <a href="https://gc.synxis.com/rez.aspx?Hotel=61860&chain=16500&template=RBE&shell=RBE&Filter=META&locale=EN-US&currency=GBP&Rooms=1&adult=2&child=0&start=availresults&arrive=11%2F04%2F2018&depart=11%2F05%2F2018&utm_medium=metasearch&utm_campaign=meta&SRC=DERBYSOFT-GOOGLEMAP&utm_source=GoogleMap&channelCode=GOOGLE&cid=META:MondrianLondon:GoogleMAP" target="_blank">Mondrian London</a> on the south bank, an 11 minute walk to the venue across Blackfriars Bridge (for the discount from online flexi rates, enter discount code DSMERM).</p>
        <p>If you want to stay in bed a few minutes longer, <a href="https://www.crowneplaza.com/hotels/gb/en/london/loncy/hoteldetail?qAdlt=1&qBrs=6c.hi.ex.rs.ic.cp.in.sb.cw.cv.ul.vn.ki.va.vx.sp.nd.ct&qChld=0&qFRA=1&qGRM=0&qIta=99603195&qPSt=0&qRRSrt=rt&qRef=df&qRms=1&qRpn=1&qRpp=20&qSHp=1&qSmP=3&qSrt=sBR&qWch=0&srb_u=1&icdv=99603195&gclid=Cj0KCQjw08XeBRC0ARIsAP_gaQCvp13854yG1FI7AMMb1CpG41BhywburcOycURByOIIB0aeG1q8pmcaAoplEALw_wcB&dp=true&cid=41478&glat=SEAR&setPMCookies=true" target="_blank">the Crowne Plaza</a> is recommended as it’s a mere 4 minute walk to the venue. Exactly the same distance, from a slightly different direction, is <a href="https://www.grangehotels.com/hotels-london/grange-st-pauls/" target="_blank">the Grange St Paul’s</a>. Also near St Paul’s is the chic <a href="https://clubquartershotels.com/locations/club-quarters-hotel-st-pauls-london" target="_blank">Club Quarters</a>. Finally, about 10 minutes away, but closer to West End theatres and Covent Garden is the <a href="https://www.apexhotels.co.uk/apex-temple-court-hotel" target="_blank">Apex Temple Court hotel</a>.</p>
        <p>With so much to do in London, there’s no chance you’ll have a spare moment. And the friendly CoinGeek team will be on hand to offer advice if you want any recommendations.</p>
    </div>

</div>