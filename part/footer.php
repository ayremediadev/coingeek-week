<footer class="footer">
	<div class="inner">
	<!--
		<div class="footer__info">
			<div class="panel panel--default panel--id-9843">
				<div class="panel__header">
					<h4 class="panel__header__title">Coingeek Conference </h4>
				</div>
				<div class="panel__body">
					<p><a href="#about" target="_self" title="about-the-conference">About the Conference</a></p>
					<p><a href="#agenda" target="_self" title="2017-agenda">Agenda&nbsp;</a></p>
					<p><a href="#speakers" target="_self" title="2018-speakers">Speakers</a></p>
					<p><a href="#buy-pass" target="_self" title="buy-a-pass">Book a Pass</a></p>
				</div>
			</div>
			<div class="panel panel--default panel--id-9844">
				<div class="panel__header">
					<h4 class="panel__header__title"> Coingeek contact </h4>
				</div>
				<div class="panel__body">
					<p><a href="mailto:hello@coingeek.com?Subject=Coingeek%20Conference" target="_top" title="hello@coingeek.com">hello@coingeek.com</a></p>
					<p>+44(0)111 222 333</p>
				</div>
			</div>
			<div class="panel panel--default panel--id-9845">
				<div class="panel__header">
					<h4 class="panel__header__title">About</h4>
				</div>
				<div class="panel__body">
					<p><a href="#why-enter" target="_self" title="why-enter">Why enter</a></p>
					<p><a href="#categories" target="_self" title="categories">Categories</a></p>
					<p><a href="#how-to-enter" target="_self" title="how-to-enter">How to enter</a>​​​​​​​</p>
				</div>
			</div>
			<div class="panel panel--default panel--id-9846">
				<div class="panel__header">
					<h4 class="panel__header__title"> Conference Contact </h4>
				</div>
				<div class="panel__body">
					<p><a href="mailto:hello@coingeek.com?Subject=Coingeek%20Conference" target="_top" title="hello@coingeek.com">hello@coingeek.com</a></p>
					<p>+44(0)111 222 333</p>
				</div>
			</div>
		</div>-->
		<div class="footer__branding">
			<div class="panel panel--default panel--id-9847">
				<div class="panel__body">
					<p><img alt="Coingeek Conference" src="/dist/img/logo.png" style="width: 309px; height: 68px;" /></p>
				</div>
			</div>
		</div>
		<div class="footer__organiser-accreditation">
			<div class="footer__organiser">
				<div class="footer__organiser__links">
					<ul class="menu menu--links">
						<li title=" Copyright © 2018 Centaur Communications Limited." class="menu__item">
							<span class="menu__item__text"> Copyright © 2018 Coingeek. All Rights Reserved.</span>
						</li>
						<li title="Terms &amp; Conditions" class="menu__item">|
							<a href="https://coingeek.com/terms-of-use/" target="_blank" class="menu__item__link">Terms & Conditions</a>
						</li>
						<li title="Privacy Policy" class="menu__item">|
							<a href="https://coingeek.com/privacy-policy/" target="_blank" class="menu__item__link">Privacy Policy</a>
						</li>
						<li title="Delegate Terms and Conditions" class="menu__item">|
							<a href="http://ayre.ag" target="_self" class="menu__item__link">An Ayre Group Property.</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="footer__accreditation">
				<div class="footer__accreditation__showoff">
					<a href="http://ayremedia.com" target="_blank">Exhibition Website by AyreMedia</a>
				</div>
				<div class="footer__accreditation__logo">
					<div class="panel panel--default panel--id-9849">
						<div class="panel__body">
							<p><img alt="AyreMedia" src="/dist/img/poweredby/logo-media-white.png" style="width: 83px; height: 85px;" /></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>