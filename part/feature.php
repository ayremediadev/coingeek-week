<div class="section section--one-column section--id-5 section--feature">
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<a class="anchor" name="Marketing Week Masters"></a> 
			<article class="article article--featured">
				<div class="article__image"> 
					<img src="__media/build_assets/img_1.jpg" alt="Marketing Week Masters" /> 
				</div>
				<div class="article--featured__content">
					<div class="article__header">
						<h3 class="article__header__title"> Coingeek Week Masters </h3>
					</div>
					<div class="article__body">
						<p>The Coingeek Week Masters&nbsp;are the awards that celebrate and reward&nbsp;mastery in marketing. It’s more than marketing excellence. It goes beyond best practice. The Masters award the work that combines creativity and innovation with effectiveness - setting new standards for the industry.</p>
						<p><a class="ck-button-three" href="masters" target="_blank" title="masters">FIND OUT MORE</a><br /> &nbsp;</p>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>