<style>
	
	.tickets_cont {
	width: 100%;
	}
	
	@media
	only screen 
	and (min-width: 760px), (max-device-width: 768px) 
	and (min-device-width: 1024px)  {
	
	#tickets .stack {
	background: none;
	}
	#tickets table * {
	border: none;
	}
	#tickets table {  
	    color: #333;
		font-family: Helvetica, Arial, sans-serif;
		margin: auto;
		border-collapse: collapse;
		border-spacing: 0;
		margin-bottom: 50px;
		width: 500px;
		transform: scale(1.1);
		-webkit-transform: scale(1.1);
		-moz-transform: scale(1.1);
		-ms-transform: scale(1.1);
		-0-transform: scale(1.1);
	}
	#tickets tr.active td{
	background-color:#251f49!important;
	color:#efb450 !important;
	position: relative;
	right: -10px;
	transform: scale(1.2);
	-webkit-transform: scale(1.2);
	-moz-transform: scale(1.2);
	}
	
	#tickets td, #tickets th {  
	border: 1px solid transparent; /* No more visible border */
	height: 30px; 
	transition: all 0.3s;  /* Simple transition for hover effect */
	}
	
	#tickets th {
	background: #251f49;
	font-weight: bold;
	color: #fff;
	padding: 0.25em 0%!important;
	}
	
	#tickets td {  
	background: #FAFAFA;
	text-align: center;
	}
	
	/* Cells in even rows (2,4,6...) are one color */        
	#tickets tr:nth-child(even) td { background: #FFFFFF; }   
	
	/* Cells in odd rows (1,3,5...) are another (excludes header cells)  */        
	#tickets tr:nth-child(odd) td { background: #FFFFFF; }  
	
	/* Hover cell effect! */
	#tickets tr td:hover {  }  
	
	/* Ribbon effect */
	#tickets tr.active td{
	background-color:#251f49!important;
	color:#efb450 !important;
	background-color: #251f49 !important;
	color: #efb450 !important;
	position: relative;
	right: -10px;
	transform: scale(1.2);
	-webkit-transform: scale(1.2);
	-moz-transform: scale(1.2);
	}
	#tickets tr.active td:last-child::after {
	content: '';
	position: fixed;
	top: -15.5px;
	right: 0px;
	border-right: 3vw solid transparent;
	border-bottom: 16px solid #fff;
	}
	
	#tickets tr.active td:first-child::after {
    content: '';
    position: absolute;
    top: -16px;
    left: 0px;
    border-left: 1vw solid transparent;
    border-bottom: 16px solid #fff;
	}
	
	}
	
	/*======\ For Mobile /======*/
	
	/*
	Max width before this PARTICULAR table gets nasty. This query will take effect for any screen smaller than 760px and also iPads specifically.
	*/
	@media
	only screen 
	and (max-width: 760px), (min-device-width: 768px) 
	and (max-device-width: 1024px)  {
	
	#tickets .stack{
		border-radius: 5px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
	}
	
	#tickets .stack td {
		padding: 5px 50px !important;
	}
	
	#tickets .table_plan{
	width:100%;
	text-align:right;
	
	}
	#tickets td{ width:100%!important; padding-left:4px;}
	
	#tickets .row-hide{ display: none;}
	
	/* Force table to not be like tables anymore */
	#tickets table, #tickets thead, #tickets tbody, #tickets th, #tickets td, #tickets tr {
	display: block;
	}
	
	/* Hide table headers (but not display: none;, for accessibility) */
	#tickets thead tr {
	position: absolute;
	top: -9999px;
	left: -9999px;
	}
	
	#tickets tr {
	margin: 0 0 1rem 0;
	}
	
	#tickets tr:nth-child(odd) {
	border-width: 1px 0;
	border-style: dashed;
	border-color: #000000;
	}
	
	#tickets td {
	/* Behave  like a "row" */
    border: none !important;
    position: relative;
	padding-left:50%;
	}
	
	#tickets td:before {
	/* Now like a table header */
	position: absolute;
	/* Top/left values mimic padding */
	top: 0;
	left: 6px;
	width: 45%;
	padding-right: 10px;
	white-space: nowrap;
	}
	
	#tickets tr.active {
    background: #032549;
    left: 10px;
    position: relative;
    color: #EFB450;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    padding: 10px 2vw 0px 10px;
	}
	
	/*
	Label the data
	You could also use a data-* attribute and content for this. That way "bloats" the HTML, this way means you need to keep HTML and CSS in sync. Lea Verou has a clever way to handle with text-shadow.
	*/
	
	#tickets td:nth-of-type(1):before { content: "Plan"; }
	#tickets td:nth-of-type(2):before { content: "One Day Ticket"; cursor:pointer; }
	#tickets td:nth-of-type(3):before { content: "All Days Ticket"; cursor:pointer; }
	#tickets td:nth-of-type(4):before { content: "bComm Associates Ticket"; cursor:pointer; }
	}
</style>
<div id="tickets" class="section section--one-column section--id-7 section--testimonials">
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<article class="article article--testimonials">
				<div class="article__body">
					<div class="w-testimonials w-testimonials--id-uid_A9C94"> 
						<div class="w-testimonials__list__item__image" style="background-image: url(/dist/img/general/tickets.jpg);"></div>
						
						<div class="w-testimonials__list__item__wrapper">
							
							<div class="tickets_cont">
								<div class="article__header">
									<h3 class="article__header__title"> Price Options </h3>	
									<p style="color:#ffffff; margin:auto; max-width:880px; ">Select a ticket option that suits you, whether you want to attend one, two or all three days. All payments are to be made by Bitcoin Cash (BCH).<br> 
										Haven’t got a wallet yet or need support with the BCH payment? Please contact our Bitcoin Cash Specialist <a href="mailto:petter@ayremedia.com">Petter</a>.<br/> 
										For debit/credit card payment, please <a title="Coin Geek Week on Eventbrite" target="_blank" href="https://www.eventbrite.co.uk/e/coingeek-week-cryptocurrency-and-blockchain-conference-tickets-49939393069">click here</a>. <br/></p>
									<div style="color:#ffffff; margin:auto; max-width:600px; ">
										<a title="Coin Geek Week on Eventbrite" target="_blank" href="https://www.eventbrite.co.uk/e/coingeek-week-cryptocurrency-and-blockchain-conference-tickets-49939393069"><img alt="Eventbrite accepts Mastercard, VISA, and American Express" src="/dist/img/rocketr/debit_credit_cards.png" /></a>
									</div>
								</div>
								
								<hr class="exp-line1">
								<hr class="exp-line2">
								<table class=" stack table_plan" role="table">
									<thead role="rowgroup">
										<tr role="row">
											<th role="columnheader" style="border-radius: 10px 0 0 0;">
												<!--
												<div class="timer1">
													
													<div id="countdown" style="color: #fff;background: red;position: relative;"></div>
													<br>
													Until the Early Bird Plan Expaires.
												</div>
												<div class="timer2">
													<div id="countdown2" style="color: #fff;background: red;position: relative;"></div>
												</div>
												<div class="timer3">
													<div id="countdown3" style="color: #fff;background: red;position: relative;"></div>
												</div>-->
											</th>
											<th role="columnheader">One Day Ticket <br><div class="plan-status">(<a href="#buy-pass">Buy now</a>)</div></th>
											<th role="columnheader">All Days Ticket <br>(<a href="#buy-pass">Buy now</a>)</th>
											<th role="columnheader" style="border-radius: 0 10px 0 0;"><a href="https://bcommassociation.com/" target="_blank">bComm Associates</a> Ticket <br>(<a href="#buy-pass">Buy now</a>)</th>
										</tr>
									</thead>
									<tbody role="rowgroup">
										<tr  class="row-hide" >
											<td ></td>
											<td ></td>
											<td ></td>
											<td ></td>
										</tr>
										<tr role="row" class="plan1" >
											<td role="cell">Early bird</td>
											<td role="cell">£ 680</td>
											<td role="cell">£ 1,275</td>
											<td role="cell">£ 1,150</td>
										</tr>
										<tr role="row" class="plan2">
											<td role="cell">Late bird</td>
											<td role="cell">£ 720</td>
											<td role="cell">£ 1,350</td>
											<td role="cell">£ 1,210</td>
										</tr>
										<tr role="row" class="plan3 active">
											<td role="cell">Standard</td>
											<td role="cell">£ 800</td>
											<td role="cell">£ 1,500</td>
											<td role="cell">£ 1,350</td>
										</tr>
									</tbody>
								</table>
								
							</div>
							
						</div>
						
					</div>
				</div>
				
			</article>
		</div>
	</div>
</div>
<script>
	
	
</script>