<div class="section section--one-column section--id-7 section--testimonials">
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<article class="article article--testimonials">
				<div class="article__wrapper">
					<div class="article__body">
						<div class="w-testimonials w-testimonials--id-uid_A9C94"> 
							<ul class="w-testimonials__list">
								<li class="w-testimonials__list__item">
									<div class="w-testimonials__list__item__image" style="background-image: url(/dist/img/general/Slide1.jpg);"></div>
									<div class="w-testimonials__list__item__wrapper">
										<div class="w-testimonials__list__item__quote">
											“We intend to enable the complete swap of goods, services, money and stock; so that in the future, you won’t even think about using Bitcoin Cash or not. <b>It will just be the way you do commerce.</b>” 
										</div>
										<div class="w-testimonials__list__item__author">
											 Dr. Craig Wright
											<div class="w-testimonials__list__item__body__company">
												Chief Scientist nChain Group
											</div>
										</div>
									</div>
								</li>
								<!--<li class="w-testimonials__list__item">
									<div class="w-testimonials__list__item__image" style="background-image: url(/dist/img/general/Slide2.jpg);"></div>
									<div class="w-testimonials__list__item__wrapper">
										<div class="w-testimonials__list__item__quote">
											“It’s not the fact that Bitcoin is called Bitcoin that is important, it is the fact that Bitcoin is usable as cash for the entire world … Nobody can block your payments or freeze your account. <b>It’s those characteristics that make Bitcoin important.</b>” 
										</div>
										<div class="w-testimonials__list__item__author">
											 Roger Ver  
											<div class="w-testimonials__list__item__body__company">
												CEO Bitcoin.com
											</div>
										</div>
									</div>
								</li>-->
								<li class="w-testimonials__list__item">
									<div class="w-testimonials__list__item__image" style="background-image: url(/dist/img/general/Slide3.jpg);"></div>
									<div class="w-testimonials__list__item__wrapper">
										<div class="w-testimonials__list__item__quote">
											“<b>Bitcoin Cash is about to become more than just cash.</b> The Bitcoin Cash blockchain now has the technical features to open the door to a wave of tokens, smart contracts, new ways to trade, new business models and, more importantly, entirely new world markets.” 
										</div>
										<div class="w-testimonials__list__item__author">
											Jimmy Nguyen 
											<div class="w-testimonials__list__item__body__company">
												CEO nChain Group
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>