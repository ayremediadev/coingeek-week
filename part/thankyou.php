<div id="buy-pass" class="cta">
	<div class="panel panel--default panel--id-9857">
		<div class="panel__header">
			<h4 class="panel__header__title"> CoinGeek Week Conference Tickets </h4>
		</div>
		<div class="panel__body">
			<p style="text-align: center;">28th-30th November 2018</p>		
		</div>
		<div class="body-pass">
			
			<p style="text-align: center;">Fill in the form below, select your preferred ticket type and proceed to make your payment.</p>
			<p style="text-align: center;">All payments are to be made by Bitcoin Cash (BCH). Haven’t got a wallet yet or need support with the BCH payment? <br> Please contact our Bitcoin Cash Specialist <a href="mailto:petter@ayremedia.com">Petter</a>.</p>
		</div>

        <div id="form-rocketr">
			
			<div class="step-prices">
			
                <div class="top-prices"></div>

                <div id="subChoice">    
                    <hr>
                    <div class="step-prices">
                        <h3 class="step-prices-t">
                            <div style="padding: 5px;">Choose Dates</div>
                        </h3>
                        <form id="singleDayPrice" action="rocketr/createOrder.php" method="POST" enctype="multipart/form-data">
                            <input name="singleDayPrice" type="hidden" value="single">
                            <input class="day1" name="Day_1" type="hidden" value="selected">
                            <input class="day2" name="Day_2" type="hidden" value="selected">
                            <input class="day3" name="Day_3" type="hidden" value="">
                            
                            <div class="tiers tier2 active">
                                <div class="pointer">
                                    <div style="">APPLICATION DAY</div>	
                                </div>
                                <div class="day-tag">DAY 1</div>
                                <br>
                                <div class="price-label"> 28th<br>
                                    <i class="currency-box">£</i>0.85				</div>
                            </div>
                            <div class="tiers tier2 active">
                                <div class="pointer">
                                    <div style="">APPLICATION MERCHANTS DAY</div>
                                </div>
                                <div class="day-tag">DAY 2</div>
                                <br>
                                <div class="price-label"> 29th<br>
                                    <i class="currency-box">£</i>0.85 
                                </div>
                            </div>
                            <div class="tiers tier3">
                                <div class="pointer">
                                    <div style="">FUTURE PROSPECTS DAY</div>
                                </div>
                                <div class="day-tag">DAY 3</div>
                                <br>
                                <div class="price-label"> 30th<br>
                                    <i class="currency-box">£</i>0.85
                                </div>
                            </div>
                            <br>
                            <div class="uk-text-center uk-margin-top">
                                <span class="input-group-btn align-center"><button href="" type="submit" class="btn btn-form btn-black display-4">Confirm</button></span>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
                <div id="ThankYou">
                    
                </div>
                <div id="subPay">
                    <hr>
                    <div id="ReferenceBlock" class="step-prices-t">
                        <div style="padding: 5px;"><b>Reference Number: CG-CONF-01fa8baa8971</b></div>
                        <div style="padding: 5px;"><b>Ticket Selected: Day 1, Day 2</b></div>
                        <div style="padding: 5px;"><b>TOTAL PRICE: £1</b></div>
                    </div>
                    <div>
                        <button class="ck-button-three btn-restart" id="restart">Restart</button>
                    </div>
                    <div> 
                        <div id="qrcode">
                            <img class="mob-code" src="/dist/img/rocketr/mob-code.png">
                            <div class="code-img">
                                <div class="code-vision">
                                    <div class="qramount">Thank you<br>Your payment was completed<br>You will receive a confirmation email soon.</div>
                                </div>
                                <div class="code-vision">
                                    <img id="qr-img" style="padding: 20px;" src="/dist/img/rocketr/paid.png">
                                </div>
                            </div>
                        </div>
                    </div>
                

                </div>

			</div>
			
			

        </div>        
	</div>
</div>