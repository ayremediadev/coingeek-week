<div class="sponsors powered">

	<div class="panel panel--default panel--id-11386">

		<div class="panel__body">

			<p>Powered By:</p>

		</div>

	</div>

	<div class="panel panel--default panel--id-9853">

		<div class="panel__body">

			<p><img alt="Ayre Group" src="/dist/img/poweredby/logo-media-white.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-9853">

		<div class="panel__body">

			<p><img alt="Ayre Group" src="/dist/img/poweredby/footer-coingeek.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-9853">

		<div class="panel__body">

			<p><img alt="Ayre Group" src="/dist/img/poweredby/logo-group-white.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-9853">

		<div class="panel__body">

			<p><img alt="Ayre Group" src="/dist/img/poweredby/footer-ca-foundation.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-9853">

		<div class="panel__body">

			<p><img alt="Ayre Group" src="/dist/img/poweredby/footer-ca.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-9853">

		<div class="panel__body">

			<p><img alt="Centbee" src="/dist/img/poweredby/footer-centbee.png" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-11386">

		<div class="panel__body">

			<p><img alt="SV Pool" src="/dist/img/poweredby/svpool-logo-white.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-11386">

		<div class="panel__body">

			<p><img alt="Bitcoin SV" src="/dist/img/poweredby/footer-bitcoinsv_01.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-11386">

		<div class="panel__body">

			<p><img alt="Squire" src="/dist/img/poweredby/footer-squire.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

	<div class="panel panel--default panel--id-11386">

		<div class="panel__body">

			<p><img alt="nChain" src="/dist/img/poweredby/footer-nchain.png" style="width: 127px; height: 80px;" /></p>

		</div>

	</div>

</div>