<div id="headliners"class="section section--one-column section--id-2 section--grey">
	<div class="section__header">
		<h2 class="section__header__title">
			<p style="text-align: center;">Previous Headliners</p>
		</h2>
	</div>
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<article class="article article--default">
				<div class="article__body">
					<p style="text-align: center;">Meet our previous speakers from the first ever CoinGeek Week Conference earlier this year. </p>
					<p>Click the links below to see the industry leaders share their visions.&nbsp;</p>
					<p style="text-align: center;">
						<br /> 
					</p>
					<div class="js-librarylistwrapper" data-totalcount="7" data-librarytitle="Speakers">
						<div class="swiper-container swiper-headliners m-speakers-list m-speakers-list--carousel-squares">
							<div class="swiper-wrapper ">
								
								<!-- <div class="swiper-slide">
									<div class="m-speakers-list__items__item__wrapper">
										<div class="cube-container">
											<div class="thumb-cube">
												<img src="/dist/img/headliners/amaury-sechet.jpg" />
											</div>
											<div id="cube">
												<figure class="face front"></figure>
												<figure class="face back"></figure>
												<figure class="face left"></figure>
												<figure class="face right"></figure>
												<figure class="face top"></figure>
												<figure class="face bottom"></figure>
											</div>
										</div>
										<div class="m-speakers-list__items__item__header-body">
											<div class="m-speakers-list__items__item__header">
												<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry"> Amaury Sechet  </a> </h3>
												<div class="m-speakers-list__items__item__header__meta"> 
													<span class="m-speakers-list__items__item__header__meta__position"> Lead Developer<br>Bitcoin ABC  </span> 
												</div>
											</div>
											<div class="m-speakers-list__items__item__body">
												<div class="m-speakers-list__items__item__body__social">
													<ul class="ck-social-icons">
														<li><a href="https://www.youtube.com/watch?v=VDmJcloSiCs" target="_blank">youtube</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div> -->
								
								<div class="swiper-slide">
									<div class="m-speakers-list__items__item__wrapper">
										<div class="cube-container">
											<div class="thumb-cube">
												<img src="/dist/img/headliners/jimmy.jpg" />
											</div>
											<div id="cube">
												<figure class="face front"></figure>
												<figure class="face back"></figure>
												<figure class="face left"></figure>
												<figure class="face right"></figure>
												<figure class="face top"></figure>
												<figure class="face bottom"></figure>
											</div>
										</div>
										<div class="m-speakers-list__items__item__header-body">
											<div class="m-speakers-list__items__item__header">
												<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry"> Jimmy Nguyen  </a> </h3>
												<div class="m-speakers-list__items__item__header__meta"> 
													<span class="m-speakers-list__items__item__header__meta__position"> CEO - nChain Group </span> 
												</div>
											</div>
											<div class="m-speakers-list__items__item__body">
												<div class="m-speakers-list__items__item__body__social">
													<ul class="ck-social-icons">
														<li><a href="https://www.youtube.com/watch?v=7j1K0csDoHs" target="_blank">youtube</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>														
								
								<div class="swiper-slide">
									<div class="m-speakers-list__items__item__wrapper">
										<div class="cube-container">
											<div class="thumb-cube">
												<img src="/dist/img/headliners/craig-wright.jpg" />
											</div>
											<div id="cube">
												<figure class="face front"></figure>
												<figure class="face back"></figure>
												<figure class="face left"></figure>
												<figure class="face right"></figure>
												<figure class="face top"></figure>
												<figure class="face bottom"></figure>
											</div>
										</div>
										<div class="m-speakers-list__items__item__header-body">
											<div class="m-speakers-list__items__item__header">
												<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry"> Craig Wright  </a> </h3>
												<div class="m-speakers-list__items__item__header__meta"> 
													<span class="m-speakers-list__items__item__header__meta__position"> Chief Scientist, nChain </span> 
												</div>
											</div>
											<div class="m-speakers-list__items__item__body">
												<div class="m-speakers-list__items__item__body__social">
													<ul class="ck-social-icons">
														<li><a href="https://www.youtube.com/watch?v=uuoVipRWd_s" target="_blank">youtube</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<!--<div class="swiper-slide">
									<div class="m-speakers-list__items__item__wrapper">
										<div class="cube-container">
											<div class="thumb-cube">
												<img src="/dist/img/headliners/roger-ver.jpg" />
											</div>
											<div id="cube">
												<figure class="face front"></figure>
												<figure class="face back"></figure>
												<figure class="face left"></figure>
												<figure class="face right"></figure>
												<figure class="face top"></figure>
												<figure class="face bottom"></figure>
											</div>
										</div>
										<div class="m-speakers-list__items__item__header-body">
											<div class="m-speakers-list__items__item__header">
												<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry"> Roger Ver  </a> </h3>
												<div class="m-speakers-list__items__item__header__meta"> 
													<span class="m-speakers-list__items__item__header__meta__position"> CEO - Bitcoin.com  </span> 
												</div>
											</div>
											<div class="m-speakers-list__items__item__body">
												<div class="m-speakers-list__items__item__body__social">
													<ul class="ck-social-icons">
														<li><a href="https://www.youtube.com/watch?v=TMxQiZ-XpKk" target="_blank">youtube</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>-->
								
								<div class="swiper-slide">
									<div class="m-speakers-list__items__item__wrapper">
										<div class="cube-container">
											<div class="thumb-cube">
												<img src="/dist/img/headliners/Ryan_X_Charles.jpg" />
											</div>
											<div id="cube">
												<figure class="face front"></figure>
												<figure class="face back"></figure>
												<figure class="face left"></figure>
												<figure class="face right"></figure>
												<figure class="face top"></figure>
												<figure class="face bottom"></figure>
											</div>
										</div>
										<div class="m-speakers-list__items__item__header-body">
											<div class="m-speakers-list__items__item__header">
												<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry"> Ryan X. Charles  </a> </h3>
												<div class="m-speakers-list__items__item__header__meta"> 
													<span class="m-speakers-list__items__item__header__meta__position"> Co-founder & CEO - Yours.org </span> 
												</div>
											</div>
											<div class="m-speakers-list__items__item__body">
												<div class="m-speakers-list__items__item__body__social">
													<ul class="ck-social-icons">
														<li><a href="https://www.youtube.com/watch?v=sh4G_F08dP8" target="_blank">youtube</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								
								
								
								
								
								<div class="swiper-slide">
									<div class="m-speakers-list__items__item__wrapper">
										<div class="cube-container">
											<div class="thumb-cube">
												<img src="/dist/img/headliners/Jerry_Chan.jpg" />
											</div>
											<div id="cube">
												<figure class="face front"></figure>
												<figure class="face back"></figure>
												<figure class="face left"></figure>
												<figure class="face right"></figure>
												<figure class="face top"></figure>
												<figure class="face bottom"></figure>
											</div>
										</div>
										<div class="m-speakers-list__items__item__header-body">
											<div class="m-speakers-list__items__item__header">
												<h3 class="m-speakers-list__items__item__header__title"> <a class="m-speakers-list__items__item__header__title__link js-librarylink-entry"> Jerry Chan  </a> </h3>
												<div class="m-speakers-list__items__item__header__meta"> 
													<span class="m-speakers-list__items__item__header__meta__position">  CDAS - SBI BITS <br> CSO - SBI Crypto </span> 
												</div>
											</div>
											<div class="m-speakers-list__items__item__body">
												<div class="m-speakers-list__items__item__body__social">
													<ul class="ck-social-icons">
														<li><a href="https://www.youtube.com/watch?v=y7yx1GD3n_8 " target="_blank">youtube</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							<!-- Add Pagination -->
							<div class="swiper-pagination"></div>
							<!-- Add Arrows -->
							<div class="swiper-button-next"></div>
							<div class="swiper-button-prev"></div>
						</div>
					</div>
					<br /> &nbsp;
					
					<!-- <p style="text-align: center;"><a class="ck-button-one" href="headline-stage-hall-of-fame" target="_self" title="headline-stage-hall-of-fame">VIEW ALL&nbsp;HEADLINERS</a></p> -->
				</div>
			</article>
		</div>
	</div>
	</div>	