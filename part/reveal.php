<div class="reveal">
	<div class="panel panel--slide-up panel--slide-up--gold i-attend">
		<div class="panel__image" style="background-image: url('/dist/img/general/location.jpg');"> 
			<img src="/dist/img/general/location.jpg" alt="Attend" /> 
		</div>
		<div class="panel__header-body">
			<div class="panel__header">
				<h4 class="panel__header__title"> Location </h4>
			</div>
			<div class="panel__body">
				<p>The Mermaid,<br>
				Puddle Dock<br>
				London<br> EC4V 3DB
				</p>
				<p><a class="ck-button-one" href="#buy-pass" target="_self" >BUY TICKET</a></p>
				<br>
			</div>
		</div>
	</div>
	
	<div class="panel panel--slide-up panel--slide-up--gold i-agenda">
		<div class="panel__image" style="background-image: url('/dist/img/general/Agenda2.jpg');"> 
			<img src="/dist/img/general/Agenda2.jpg"  /> 
		</div>
		<div class="panel__header-body">
			<div class="panel__header">
				<h4 class="panel__header__title"> Agenda </h4>
			</div>
			<div class="panel__body">
				<p>Discover what’s on at the London conference in November</p>
				<p><a class="ck-button-one" href="#agenda" target="_self" >FIND OUT MORE</a></p>
				<br>
			</div>
		</div>
	</div>

	<div class="panel panel--slide-up panel--slide-up--gold i-tickets">
		<div class="panel__image" style="background-image: url('/dist/img/general/tickets.jpg');"> 
			<img src="/dist/img/speakers/Speakers2.jpg" /> 
		</div>
		<div class="panel__header-body">
			<div class="panel__header">
				<h4 class="panel__header__title"> Tickets </h4>
			</div>
			<div class="panel__body">
				<p>See ticket options and prices</p>
				<p><a class="ck-button-one" href="#tickets" target="_self" >PRICES</a></p>
				<br>
			</div>
		</div>
	</div>
	
	<div class="panel panel--slide-up panel--slide-up--gold i-speakers">
		<div class="panel__image" style="background-image: url('/dist/img/speakers/Speakers2.jpg');"> 
			<img src="/dist/img/speakers/Speakers2.jpg" /> 
		</div>
		<div class="panel__header-body">
			<div class="panel__header">
				<h4 class="panel__header__title"> Speakers </h4>
			</div>
			<div class="panel__body">
				<p>Have a look at the 2018&nbsp;speaker line-up.</p>
				<p><a class="ck-button-one" href="#headliners" target="_self">FIND OUT MORE</a></p>
				<br>
			</div>
		</div>
	</div>
	
	<div class="panel panel--slide-up panel--slide-up--gold i-sponsors">
		<div class="panel__image" style="background-image: url('/dist/img/sponsors/Sponsors2.jpg');"> 
			<img src="/dist/img/sponsors/Sponsors2.jpg"  /> 
		</div>
		<div class="panel__header-body">
			<div class="panel__header">
				<h4 class="panel__header__title"> Sponsors </h4>
			</div>
			<div class="panel__body">
				<p>See sponsors and find out how you can become one.</p>
				<p><a class="ck-button-one" href="#sponsors" target="_self" >FIND OUT MORE</a></p>
				<br>
			</div>
		</div>
	</div>

</div>