<div id="sponsors"class="section section--one-column section--id-4 section--sponsors" style="padding-top: 0; background: #e6ecef; color: #000;">
	<div class="section__header">
		<h2 class="section__header__title" style="color: #000;">
			<!--<p style="text-align: center;">Would you like to <br>  sponsor CoinGeek Week?</p>-->
			<p style="text-align: center;">CoinGeek Sponsors</p>
		</h2>
	</div>

	<div class="sponsors powered section__body" style="background: none; padding-top: 0;">
	
		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="Centbee" target="_blank" href="https://centbee.com/"><img alt="Centbee" src="/dist/img/poweredby/CentbeeTransparent1-300x239.png" style="width: 127px; height: 80px;" /></a></p>

			</div>
			
		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="SV Pool" target="_blank" href="https://svpool.com/"><img alt="SV Pool" src="/dist/img/poweredby/svpool-logo-color.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="Bitcoin SV" target="_blank" href="https://bitcoinsv.io/contact/"><img alt="Bitcoin SV" src="/dist/img/poweredby/footer-bitcoinsv-color.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="Squire" target="_blank" href="http://squiremining.com/"><img alt="Squire" src="/dist/img/poweredby/footer-squire-color.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="nChain" target="_blank" href="https://nchain.com/"><img alt="nChain" src="/dist/img/poweredby/footer-nchain-color.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="CopPay" target="_blank" href="https://coppay.io/"><img alt="nChain" src="/dist/img/poweredby/coppay-footer.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="The White Company" target="_blank" href="https://thewhitecompanyus.com/"><img alt="The White Company" src="/dist/img/poweredby/footer-white-color.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="CoinPoint" target="_blank" href="https://www.coinpoint.net/"><img alt="CoinPoint" src="/dist/img/poweredby/CoinPointLogo.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

		<div class="panel panel--default panel--id-11386">

			<div class="panel__body">

				<p><a title="Blockchain Domes" target="_blank" href="http://www.blockchaindomes.com/"><img alt="Blockchain Domes" src="/dist/img/poweredby/Artboard1copy.png" style="width: 127px; height: 80px;" /></a></p>

			</div>

		</div>

	</div>

	<div class="section__body" style="padding: 20px;max-width: 1000px;margin: auto; font-weight: 900; color: #000;">
		<br/>
		<p style="text-align: justify;font-size: 22px;">
			The CoinGeek Conference is an extremely fast-growing cryptocurrency and blockchain conference, which was first launched in Hong Kong in May 2018. Due to popular demand, it is coming to London in November and has grown from a one-day to a three-day event.
		</p>
		<p style="text-align: justify;font-size: 22px;">
			During the three days we will be hosting talks, debates and Q&As with the industry leaders.
		</p>
		<p style="text-align: justify;font-size: 22px;">
			Do you want to display your brand in front of hundreds of dedicated app developers, merchants and miners? To discuss sponsorship opportunities, send an email to our consultant: <a href="mailto:petter@ayremedia.com">petter@ayremedia.com</a>. 
		</p>
		<p style="text-align: center;">
			<a class="ck-button-one" href="/files/CG-sponsorship.pdf" style="color: #fff; font-size: 16px;" target="_blank" title="find-out-more">Find out more</a>
		</p>
	</div>

</div>