<!-- Desktop Menu-->
<div class="header-container">
	
	<!-- HAmburger Menu icon -->
	<div id="hamburger" class="hamburglar is-closed">
		<div class="burger-icon">
			<div class="burger-container"> <span class="burger-bun-top"></span> <span class="burger-filling"></span> <span class="burger-bun-bot"></span> </div>
		</div>
		
		<!-- svg ring containter -->
		<div class="burger-ring"> 
			<svg class="svg-ring">
				<path class="path" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="4" d="M 34 2 C 16.3 2 2 16.3 2 34 s 14.3 32 32 32 s 32 -14.3 32 -32 S 51.7 2 34 2" />
			</svg> 
		</div>
		<!-- the masked path that animates the fill to the ring --> 
		
		<svg width="0" height="0">
			<mask id="mask">
				<path xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#ff0000" stroke-miterlimit="10" stroke-width="4" d="M 34 2 c 11.6 0 21.8 6.2 27.4 15.5 c 2.9 4.8 5 16.5 -9.4 16.5 h -4" />
			</mask>
		</svg>
		<div class="path-burger">
			<div class="animate-path">
				<div class="path-rotation"></div>
			</div>
		</div>
	</div>
	
	<div class="inner">	
		<div class="header-logo">
			<div class="panel panel--default panel--id-9819"> 
				<div class="panel__body"> 
					<a href="https://coingeekweek.com/" target="_self" title="welcome"><img alt="Coingeek Conference" src="/dist/img/logo.png" style="width: 264px; height: 85px;" /></a> 
				</div> 
			</div>
		</div>
		
		<div class="inner-right">
			<nav class="navigation js-navigation" data-name="main" >
				<ul class="menu menu--dropdown js-menu js-menu-dropdown"><li title="Home" class="is-active menu__item menu__item--mobile">
					<li class="menu__item">
						<a href="https://coingeekweek.com/#headliners" target="_self" class="menu__item__link">Headliners</a>
					</li>
					<!--
					<li class="menu__item">
						<a href="#speakers" target="_self" class="menu__item__link">Speakers</a>
					</li>
					-->
					<li class="menu__item">
						<a href="https://coingeekweek.com/#sponsors" target="_self" class="menu__item__link">Sponsors</a>
					</li>
					<li class="menu__item">
						<a href="https://coingeekweek.com/#minersday" target="_self" class="menu__item__link">Miners Day</a>
					</li>
					<li class="menu__item">
						<a href="https://coingeekweek.com/#agenda" target="_self" class="menu__item__link">Agenda</a>
						<ul class="menu--sub-menu menu--sub-menu--level-1">
							<!--
								<li title="Get in touch" class="menu__item">
								<a href="get-in-touch" target="_self" class="menu__item__link">Get in touch</a>
								</li>
								<li title="FAQs" class="menu__item">
								<a href="faqs" target="_self" class="menu__item__link">FAQs</a>
								</li>
							-->
						</ul>
					</li>

					<li class="menu__item">
						<a class="ck-button-one" href="https://coingeekweek.com/#buy-pass" title="buy-a-pass">BUY TICKET</a>
					</li>
				</ul>
				</nav>
				
				<div class="header-social">
					<div class="panel panel--default panel--id-9820"> 
						<div class="panel__body"> 
							<ul class="ck-social-icons"> 
								<li><a href="https://www.youtube.com/channel/UC95_Nqes9m5arhoT1lt1SFg" target="_blank" title="https://www.youtube.com/channel/UC95_Nqes9m5arhoT1lt1SFg">Youtube</a></li> 
								<li><a href="https://twitter.com/realcoingeek" target="_blank" title="https://twitter.com/realcoingeek">Twitter</a></li> 
								<li><a href="https://www.facebook.com/realcoingeek/" target="_blank" title="https://www.facebook.com/realcoingeek/">Facebook</a></li> 
							</ul> 
						</div> 
					</div>
				</div>
			</div>	
		</div>
	</div>						