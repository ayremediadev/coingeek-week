<?php
   	/* ====================================================================*\
		*  (c) Copyright 2017 Francesco Sorrentino. All rights reserved. *
		*  License & Distribution of the same is forbidden.              *
		*  https://sorrentino.ga                                         *
	\* ====================================================================*/
	session_start();
    session_cache_limiter( 'nocache' );
	
    header( 'Expires: ' . gmdate( 'r', 0 ) );
    header( 'Content-type: application/json' );
	
	
	//===================================================================
	//Prepare Fields
	//----------------------------
    $to         = 'chris@ayremedia.com';  // put your email here
    //$to         = 'francesco@ayremedia.com,emmanuel@ayremedia.com,mark@ayremedia.com';  // put your email here
	
    $email_template_Admin = 'admin.html';
    //$email_template_Sender = 'sender.html';
	
    if($_POST['subject'] == false){ $subject='Someone Register his profile on ' . $_SERVER['SERVER_NAME'];}
    else{ $subject    = strip_tags($_POST['subject']); }
	
	$firstname  	= strip_tags($_POST['firstname']);
	$surname   	 	= strip_tags($_POST['surname']);
	$fullname   	= $firstname .' '.$surname;
	$email      	= strip_tags($_POST['email']);
	$confirmEmail	= strip_tags($_POST['confirmEmail']);
	$company    	= strip_tags($_POST['company']);
	$industry    	= strip_tags($_POST['industry']);
	
    $phone      	= strip_tags($_POST['phone']);
    $message    	= nl2br( htmlspecialchars($_POST['message'], ENT_QUOTES) );
	
	if(isset($_POST['merchant'])){$merchant = strip_tags($_POST['merchant']).', ';}else{ $merchant = '';}
	if(isset($_POST['developer'])){$developer = strip_tags($_POST['developer']).', ';}else{$developer ='';}
	if(isset($_POST['miner'])){$miner = strip_tags($_POST['miner']).', ';}else{$miner ='';}
	if(isset($_POST['blockchain'])){$blockchain = strip_tags($_POST['blockchain']).', ';}else{$blockchain ='';}
	if(isset($_POST['exchange'])){$exchange = strip_tags($_POST['exchange']).', ';}else{$exchange ='';}
	if(isset($_POST['venture'])){$venture = strip_tags($_POST['venture']).', ';}else{$venture ='';}
	if(isset($_POST['media'])){$media = strip_tags($_POST['media']).', ';}else{$media ='';}
	if(isset($_POST['investor'])){$investor = strip_tags($_POST['investor']).', ';}else{$investor ='';}
	
	$categories = $merchant.$developer.$miner.$blockchain.$exchange.$venture.$media.$investor;
	$categories = rtrim($categories,", ");
	
	if(isset($_POST['subscribe'])){$subscribe  = 'Yes';}else{$subscribe  = 'No';}
	
	$tierType = "tier2";
	
	$date = date('Y-m-d H:i');
	//===========================================================================================================
    $result     = array();
	
	//Start ReCaptcha---------------------------------------->
	if(isset($_POST)){		
		$captcha=$_POST['g-recaptcha-response'];
		$ip = $_SERVER['REMOTE_ADDR'];
		$secretkey = "6LfN9XIUAAAAAJEc6T_zvmp26av8bg83-AbrAyJr";
		//Check on php.ini that allow_url_fopen=On is set
		$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$captcha."&remoteip=".$ip);
		$responseKeys = json_decode($response,true);	                     
	}
	//End ReCaptcha------------------------------------------>
	
	//===================================================================
	//Validate fields 
	
	// function checkEmail($email) {
	// if ( strpos($email, '@') !== false ) {
	// $split = explode('@', $email);
	// return (strpos($split['1'], '.') !== false ? true : false);
	// }
	// else {
	// return false;
	// }
	// }
	// $checkEmail = checkEmail($email);
	
	
    // if(empty($firstname)){
	// $result = array( 'response' => 'error', 'empty'=>'firstname', 'message'=>'<strong>Error!</strong>&nbsp; First Name is empty.' );
	// echo json_encode($result );
	// die;
	// } 
	
	// if(empty($surname)){
	
	// $result = array( 'response' => 'error', 'empty'=>'surname', 'message'=>'<strong>Error!</strong>&nbsp; Last Name is empty.' );
	// echo json_encode($result );
	// die;
	// } 
	
	//---------------------------------------------
	//Emails
    // if(empty($email)){
	// $result = array( 'response' => 'error', 'empty'=>'email', 'message'=>'<strong>Error!</strong>&nbsp; Email is empty.' );
	// echo json_encode($result );
	// die;
	// } 
	// if($confirmEmail !== $email ){
	
	// $result = array( 'response' => 'error', 'empty'=>'email', 'message'=>'<strong>Error!</strong>&nbsp; Email Confirmation field does not match your email.' );
	// echo json_encode($result );
	// die;
	// }
	// if(checkEmail == false ){
	
	// $result = array( 'response' => 'error', 'empty'=>'email', 'message'=>'<strong>Error!</strong>&nbsp; The Email Provided is not valid.' );
	// echo json_encode($result );
	// die;
	// } 
	//---------------------------------------------
	
	// $phone is valid?
	// if( !empty($phone) ){
	// if(preg_match("/^[0-9*#+]+$/", $phone)== false){
	
	// $result = array( 'response' => 'error', 'empty'=>'phone', 'message'=>'<strong>Error!</strong>&nbsp; The Phone Number Provided is not valid or contain empty spaces.' );
	// echo json_encode($result );
	// die;
	// } 
	// }
	
	//===================================================================================
	// Check if email exist in bcomm association Database
	function check_bcomm_assoc($email){
		//----------------------------
		$servername = "localhost:3306";
		$username = "root";
		$password = "$7w(B?,wLZ2AH8F~/";
		$dbname = "bcommass_temp";
		
		$mysqli = new mysqli($servername , $username, $password, $dbname);
		$result = $mysqli->query("SELECT email FROM subscribers WHERE email = '$email'");
		if($result->num_rows == 0) {
			// row not found, do stuff...
			return false;
			} else {
			// do other stuff...
			return true;
		}
		$mysqli->close();
	}
	if( check_bcomm_assoc($email) == true){
		$bcomm_assoc_status = 'Yes';
		$discount = true;
	}
	else{ 
		$bcomm_assoc_status = 'No';
		$discount = false;
	}
	
	//===================================================================================
	
	if(intval($captcha == '')) {
		$result = array( 'response' => 'error', 'message'=>'<strong>Please verify you are human!</strong>' );
		echo json_encode($result );
		die;
	}
	else{  //recaptcha ON/Off
		
		//===================================================================================
		// Create Form Sessions values which will remain through the steps
		$_SESSION['form']['adminEmail']			= $to;
		$_SESSION['form']['subject'] 			= $subject;
		$_SESSION['form']['firstname']  		= $firstname;
		$_SESSION['form']['fullname']  	 		= $fullname;
		$_SESSION['form']['email']  			= $email;
		$_SESSION['form']['company']    		= $company;
		$_SESSION['form']['industry']    		= $industry;
		$_SESSION['form']['message']   			= $message;
		$_SESSION['form']['bcomm_assoc_status'] = $bcomm_assoc_status;
		$_SESSION['form']['subscribe'] 			= $subscribe;
		$_SESSION['form']['phone']    			= $phone;
		$_SESSION['form']['categories'] 		= $categories;
		$_SESSION['form']['discount'] 		    = $discount;
		
		//===================================================================================
		//Save on Database
		//----------------------------
		$servername = "localhost:3306";
		$username = "root";
		$password = "$7w(B?,wLZ2AH8F~/";
		$dbname = "coingeek_week_temp";
		
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			
			$result = array( 'response' => 'error', 'message'=>'<strong>Error!</strong>&nbsp; Connection failed:' . $conn->connect_error );
		}
		
		$sql = "INSERT INTO attendees (fullname,company,email,phone,message,categories,bcomm_assoc_status,subscribe,date)
		VALUES ('$fullname','$company','$email','$phone','$message','$categories','$bcomm_assoc_status','$subscribe','$date')";
		//Save
		$saveOnDB = $conn->query($sql);
		
		if ($saveOnDB === TRUE) {
			//$result = array( 'response' => 'success', 'message'=>'<strong>Record Saved On Our Database.</strong>' );
		} 
		else {
			$result = array( 'response' => 'error', 'message'=>'<strong>Error!</strong>&nbsp; We are experiencing some Problem, Please try again later.'  );
		}
		
		//===================================================================================
		//Send Email
		//----------------------------
		// $headers  = "From: coingeekweek.com\r\n";
		// $headers .= "Reply-To: ". $email . "\r\n";
		// $headers .= "MIME-Version: 1.0\r\n";
		// $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		
		$headers = 	'From: coingeekweek.com<conference@coingeek.com>' . "\r\n";
		$headers .=	'Reply-To:  conference@coingeek.com'. "\r\n" ;
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= 'Content-Type: text/html; charset=UTF-8\r\n';
		
		$templateTags =  array(
		'{{subject}}' 	 			=> $subject,
		'{{firstname}}'  			=> $firstname,
		'{{fullname}}'   			=> $fullname,
		'{{email}}'      			=> $email,
		'{{company}}'    			=> $company,
		'{{message}}'    			=> $message,
		'{{bcomm_assoc_status}}' 	=> $bcomm_assoc_status,
		'{{subscribe}}'  			=> $subscribe,
		'{{phone}}'      			=> $phone,
		'{{categories}}' 			=> $categories
		);
		
		//Admin
		$templateAdmin = file_get_contents( dirname(__FILE__) . '/email-templates/'.$email_template_Admin);
		$contentsAdmin =  strtr($templateAdmin, $templateTags);
		$sendToAdmin = mail( $to, $subject, $contentsAdmin, $headers);
		
		//Notification Sender
		//$templateSender = file_get_contents( dirname(__FILE__) . '/email-templates/'.$email_template_Sender);
		//$contentsSender =  strtr($templateSender, $templateTags);
		//$sendToSender = mail( $to, $subject, $contentsSender, $headers);
		
		if ( $sendToAdmin ) {
			//===================================================================================
			// Check on what tier the form is 
			//----------------------
			//Standard Tickets
			$tierTitle		= 'Standard';
			$percentDisc	= '';
			$allDaysPrice 	= 1500;
			$oneDayPrice  	= 800;
			
			
			if($tierType == 'tier1'){
			    $tierTitle;
				$percentDisc;
				$allDaysPrice;
				$oneDayPrice;
				$savingsOne      = "Standard Price";
				$savingsAll      = "Standard Price";
			}
			elseif($tierType == 'tier2'){
				$tierTitle;
				$percentDisc;
				$allDaysPrice;
				$oneDayPrice;
				$savingsOne      = "Standard Price";
				$savingsAll      = "Standard Price";
			}
			elseif($tierType == 'tier3'){
				$tierTitle;
				$percentDisc;
				$allDaysPrice;
				$oneDayPrice;
				$savingsOne      = "Standard Price";
				$savingsAll      = "Standard Price";
			}
			//----------------------
			//bComm Tickets
			if($discount == 'Yes'){
				$allDaysPrice10 = ($allDaysPrice*10)/100;
				$oneDayPrice10  = ($oneDayPrice*10)/100;
				
				$allDaysPrice = $allDaysPrice - $allDaysPrice10;
				$oneDayPrice  = $oneDayPrice  - $oneDayPrice10;
				$bcommMSG='<h4 style="color: #93c90e;background: #000;padding: 20px;">Our system matched your email to being a member of the bCommAssociation, this entitles you to an extra price reduction of 10% on your ticket.</h4>';
				
			}
			else{
			    $allDaysPrice;
				$oneDayPrice;
				$bcommMSG='';
			}
			//Save prices in sessions
			$_SESSION['price']['tierTitle'] 	= $tierTitle;
			$_SESSION['price']['oneDayPrice'] 	= $oneDayPrice;
			$_SESSION['price']['allDaysPrice'] 	= $allDaysPrice;
			
			
			$selectPriceType='
			
			<div class="step-prices">
			
			<div class="top-prices">
			
			<h3 class="step-prices-t" >
			<div style="padding: 5px;">'
			.$tierTitle.$percentDisc.'
			</div>
			</h3>
			<br>'
			.$bcommMSG.'
			<br>
			<h2 style="color: #ffffff;">Choose A Ticket Option:</h2>
			
			
			
			
			<form id="selectSinglePrice" class="tiers mains main1 one-day-price"  action="rocketr/selectSinglePrice.php" method="POST" enctype="multipart/form-data">
			<input name="selectSinglePrice" type="hidden" value="selectSinglePrice" />
			<div class="pointer">
			<div style="">ONE DAY TICKET</div>	
			</div>
			<br>
			<div class="price-label"> 
			'.$oneDayPrice.' <i class="currency-box">GBP</i>
			</div>
			'.$savingsOne.'
			</form>
			
			
			
			<form id="allDaysPrice" class="tiers mains main2 all-days-price"  action="rocketr/createOrder.php" method="POST" enctype="multipart/form-data">
			<input name="allDaysPrice" type="hidden" value="allDaysPrice" />
			<div class="pointer">
			<div style="">ALL DAYS</div>
			</div>
			<br>
			<div class="price-label"> 
			'.$allDaysPrice.' <i class="currency-box">GBP</i>
			</div>
			'.$savingsAll.'
			</form>
			
			</div>
			
			
			<div id="subChoice"></div>
			
			<div id="subPay"></div>
			</div>
			
			
			<script>
			// Step 2 Choose Tier / On click submit
			$("#selectSinglePrice").on("click",function(){
			var $action = $(this).prop("action");
			var $data = $(this).serialize();
			var $this = $(this);	
			if (!$this.hasClass("active")) {
			$.ajax({ //Seconds Request
			type :"POST",
			url : $action,
			data: $data,     
			cache: false,
			success: function($data){                          
			$("#subChoice").html($data).show();      
			}           
			});
			
			$("#subPay").html("");
			}
			else{$("#subChoice").html("");}
			});
			// Step 4 Choose Tier / On click submit
			$("#allDaysPrice").on("click",function(){
			
			var $action = $(this).prop("action");
			var $data = $(this).serialize();
			var $this = $(this);
			if (!$this.hasClass("active")) {
			$.ajax({ //Seconds Request
			type :"POST",
			url : $action,
			data: $data,     
			cache: false,
			success: function($data){                          
			$("#subPay").html($data).show();
			}           
			});
			}
			else{
			$("#subPay").html("");
			}
			});
			
			$(".main1").on("click",function(){
			var $this = $(this);
			$this.toggleClass("active");
			$(".main2").removeClass("active");
			
			});	
			$(".main2").on("click",function(){
			var $this = $(this);
			$this.toggleClass("active");
			$(".main1").removeClass("active");
			$("#subChoice").html("");
			});	
			</script>
			';
			
			$result = array( 'response' => 'selectPriceType', 'message'=> $selectPriceType );
		} 
		else {
			$result = array( 'response' => 'error', 'message'=>'<strong>Error!</strong>&nbsp; Cann\'t Send Mail.'  );
		}
	} //recaptcha ON/Off
	echo json_encode($result );
	//die;
?>