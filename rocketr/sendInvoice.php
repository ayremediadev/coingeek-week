<?php
	//===================================================================================
	//Set Vars from Form Sessions
	session_start();
	session_cache_limiter( 'nocache' );
	
	$subject		= ''; 
	$subjectline 	= '';
	
	$adminEmail	= $_SESSION['form']['adminEmail'];
	$firstname	= $_SESSION['form']['firstname'];
	$fullname 	= $_SESSION['form']['fullname'];
	$email		= $_SESSION['form']['email'];
	$subscribe	= $_SESSION['form']['subscribe'];
	$phone 		= $_SESSION['form']['phone'];
	$discount 	= $_SESSION['form']['discount'];
	//-----------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	//Set Vars from Price Sessions
	$tierTitle	= $_SESSION['price']['tierTitle'];
	
	//-----------------------------------------------------------------------------------
	//Set Vars from Invoice Sessions
	$refNumb		= $_SESSION['invoice']['refNumb'];
	$price			= $_SESSION['invoice']['price'];
	$ticket 		= $_SESSION['invoice']['ticket'];	
	$payment_status = $_SESSION['invoice']['payment_status'];	
	$liveStatus 	= $_SESSION['invoice']['payment_status_code'];
	
	
	$sendNotification;
	If($liveStatus == 0){
		//scanning
		$sendNotification = false;
	}
	elseif($liveStatus == 1 || $liveStatus == 3 || $liveStatus == 4){
		    //ok
			$subjectadmin		= 'Payment Confirmation'; 
			$subjectlineadmin 	= 'has Purchased a Ticket.';
			
			$subject			= 'Payment Confirmation'; 
			$subjectline 		= 'Thank you for purchasing a ticket to Coingeek Conference.';
			$sendNotification 	= true;
	}
	// elseif($liveStatus == 2 ){
		// echo '2';
	// }
	else{
		//error
		$subjectadmin		= 'Payment Issue'; 
		$subjectlineadmin	= 'had issues while purchasing a ticket.'; 
		
		$subject			= 'Payment Issue'; 
		$subjectline 		= 'Your payment did not process correctly. You will receive an email from us soon to discuss the issue.'; 
		$sendNotification 	= true;
	}
	
	//echo '<pre>';
	//print_r($_SESSION);
	//echo '</pre>';
	
	if( $sendNotification ){
		//===================================================================================
		//Save on Database
		//----------------------------
		$servername = "localhost:3306";
		$username = "root";
		$password = "$7w(B?,wLZ2AH8F~/";
		$dbname = "coingeek_week_temp";
		
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			$result = '<strong>Error!</strong>&nbsp; Connection failed:' . $conn->connect_error;
		}
		
		$sql = 'UPDATE `attendees` SET `tierTitle`= "'.$tierTitle.'",`refNumb` = "'.$refNumb.'", `ticket`= "'.$ticket.'",`price` = "'.$price.'", `payment_status` = "'.$payment_status.'"
		        WHERE `email` = "'.$email.'" '; 
		
		//Save
		$saveOnDB = $conn->query($sql);
		
		// if ($saveOnDB === TRUE) {
			//$result = array( 'response' => 'success', 'message'=>'<strong>Record Saved On Our Database.</strong>' );
			// echo 'ook';
			// } else {
			//$result = array( 'response' => 'error', 'message'=>'<strong>Error!</strong>&nbsp; We are experiencing some Problem, Please try again later.'  );
			// echo 'noo';
		// }
		//$conn->close();
		
		
		//===================================================================================
		//Send Emails
		//----------------------------
		$headers = 	"From: coingeekweek.com" . "\r\n";
		$headers .=	'Reply-To: conference@coingeek.com'. "\r\n" ;
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		
		
		$templateTags =  array(
		'{{subjectadmin}}' 	 		=> $subjectadmin,
		'{{subjectlineadmin}}' 	 	=> $subjectlineadmin,
		'{{subject}}' 	 			=> $subject,
		'{{subjectline}}' 	 		=> $subjectline,
		'{{firstname}}'  			=> $firstname,
		'{{fullname}}'   			=> $fullname,
		'{{email}}'      			=> $email,
		'{{phone}}'      			=> $phone,
		'{{tierTitle}}' 			=> $tierTitle,
		'{{refNumb}}' 				=> $refNumb,
		'{{price}}' 				=> $price,
		'{{ticket}}' 				=> $ticket
		);
		
		//Confirm To Admin
		$templateAdmin = file_get_contents( dirname(__FILE__) . '/email-templates/invoiceAdmin.html');
		$contentsAdmin =  strtr($templateAdmin, $templateTags);
		$sendToAdmin = mail( $adminEmail, $subject, $contentsAdmin, $headers);
		
		//Confirm To Buyer
		$templateBuyer = file_get_contents( dirname(__FILE__) . '/email-templates/invoiceBuyer.html');
		$contentsBuyer = strtr($templateBuyer, $templateTags);
		$sendToBuyer   = mail( $email, $subject, $contentsBuyer, $headers);
		
		//if (  $sendToBuyer ) {
		//echo '<hr>Email Sent!<i style="color:green;" class="far fa-check-circle"></i>';
		//}
		
		session_destroy();
		
	}
?>