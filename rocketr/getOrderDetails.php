<?php
	session_start();
	session_cache_limiter( 'nocache' );
	
	require_once('config.php'); 
	
	if($_GET['order_id']){
	
		$order_id = $_GET['order_id'];
		
		$url='https://api.rocketr.net/orders/'.$order_id.'/details';
		
		//Using cURL
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'application-id: '.$API_ID,
		'authorization: '.$API_Secret,
		'content-type: application/json'
		));
		
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch);
		
		//echo '<pre>';
		$result = json_decode($result);
		
		//Test
		//var_dump($result);
		//echo $result->status;
		//echo json_decode($result[4]);
		//var_dump(json_decode($result, true));
		//echo '</pre>';
		
		//==========================================================
		$liveStatus = $result->status;
		$liveStatusText = $result->statusText;
		//-------------------------------------
		//Save in Sessions
		$_SESSION['invoice']['payment_status_code'] = $liveStatus;
		$_SESSION['invoice']['payment_status'] = $result->statusText;
		//===========================================================
		
		If($liveStatus == 0){
			echo '0';
		}
		elseif($liveStatus == 1 || $liveStatus == 3 || $liveStatus == 4){
		    //ok
			echo '3';
		}
		// elseif($liveStatus == 2 ){
			// echo '2';
			//We may need it later
		// }
		else{
			//error
			echo '9';
		}
		
	}
	else{
		return false;
	}
	
	
?>
