<?php
   	/* ====================================================================*\
		*  (c) Copyright 2017 Francesco Sorrentino. All rights reserved. *
		*  License & Distribution of the same is forbidden.              *
		*  https://sorrentino.ga                                         *
	\* ====================================================================*/
	//===================================================================================
	//Require lib
	require_once('init.php'); 
	require_once('config.php'); 
	
	//===================================================================================
	//Set Vars from Form Sessions
	session_start();
	session_cache_limiter( 'nocache' );
	
	//-----------------------------------------------------------------------------------
	//Set Vars from Invoice Sessions
	$fullname	= $_SESSION['form']['fullname'];
	$email		= $_SESSION['form']['email'];
	$refNumb	= $_SESSION['invoice']['refNumb'];
	$price		= $_SESSION['invoice']['price'];
	$ticket 	= $_SESSION['invoice']['ticket'];	
	//-----------------------------------------------------------------------------------
	//Set Vars from Price Sessions
	$tierTitle		= $_SESSION['price']['tierTitle'];
	$oneDayPrice 	= $_SESSION['price']['oneDayPrice'];
	$allDaysPrice	= $_SESSION['price']['allDaysPrice'];
	
	//print_r($_SESSION);
	//===================================================================================
	
	//Create Order and invoices
	if($_POST){
		//================================================
		//Determinate Price
		if($_POST['singleDayPrice']){
			
		    $selected=0;
			$ticket = '';
			
		    foreach($_POST as $name => $value ){
				if($value == 'selected'){
					if($name == 'Day_1'){$name = 'Day 1';}
					if($name == 'Day_2'){$name = 'Day 2';}
					if($name == 'Day_3'){$name = 'Day 3';}
					
					$ticket .= $name.', ';
					$selected++;
				}
			}
			//----------------------------------
			//Delete last comma
			$ticket = rtrim($ticket,", ");
			//----------------------------------
			//Moltiply price per number selected
			$price = $oneDayPrice * $selected;
		}
		if($_POST['allDaysPrice']){
			$ticket = 'All Days Price';
			$price  = $allDaysPrice;
		}
		
		//================================================
		//Create Order
		if ($price >= 1){
			
            //--------------------------------------------
			//Start Order Rocketr API CALL
			\RocketrPayments\RocketrPayments::setApiKey($API_ID, $API_Secret); // Set your API From https://rocketr.net/merchants/api-keys
			
			$o = new \RocketrPayments\Order();
			$o->setPaymentMethod(\RocketrPayments\PaymentMethods::BitcoinCashPayment); //Set the payment method here
			
			$o->setAmount( (int)$price );
			//$o->setAmount( 1.00 );
			//$o->setBuyerEmail( $email );
			
			$o->addCustomField('Full Name', $fullname );
			$o->addCustomField('Email', $email );
			$o->addCustomField('Tier', $tierTitle );
			$o->addCustomField('Ticket Type', $ticket );
			
			//$o->setIpnUrl('https://coingeekweek.com/rocketr/lib/webhook.php');
			
			$result = $o->createOrder();
			
			//End Order Rocketr API CALL
		    //--------------------------------------------
			
			//============================================
			//Ref Number 
			$refNumb = 'CG-CONF-'. $result['paymentInstructions']['orderIdentifier'];
			
			//Save invoice in sessions
			$_SESSION['invoice']['refNumb'] = $refNumb;
			$_SESSION['invoice']['price'] 	= $price;
			$_SESSION['invoice']['ticket'] 	= $ticket;			
		    //--------------------------------------------
			//Display Result Order Waiting for Payment 
			
			$status = '
			<hr>
			<div id="ReferenceBlock" class="step-prices-t" >
			<div style="padding: 5px;"><b>Reference Number: '.$refNumb.'</b></div>
			<div style="padding: 5px;"><b>Ticket Selected: '.$ticket.'</b></div>
			<div style="padding: 5px;"><b>TOTAL PRICE: '.(int)$price.' GBP</b></div>
			</div>
			<div> 
			<div class="countdown"></div>
			<div id="qrcode">
			<img class="mob-code" src="/dist/img/rocketr/mob-code.png" />
			<div class="code-img">
			<div class="code-vision">
			<div class="qramount">'.$result['paymentInstructions']['amount'] .' ' . $result['paymentInstructions']['currencyText'].'</div>
			<div class="qraddress">' . $result['paymentInstructions']['address'].'</div>
			</div>
			<div class="code-vision">
			<img id="qr-img" style="padding: 20px;" src="'.$result['paymentInstructions']['qrImage'] .'" />
			</div>
			</div>
			</div>
			</div>
			
			<script>window.location = "#subPay";</script>
			<script src="/dist/js/easytimer.min.js"></script>
			<script>
			
			
			//Start Timer
			var timer = new Timer();
			timer.start({countdown: true, startValues: {minutes:14, seconds: 59}});
			$(".countdown").html(timer.getTotalTimeValues().minutes+":"+timer.getTotalTimeValues().seconds);
			
			timer.addEventListener("secondsUpdated", function (e) {
			$(".countdown").html(timer.getTimeValues().minutes+":"+timer.getTimeValues().seconds);
			
			});
			
			//Set loop value
			var flagTimeLoop = 1;
			
			timer.addEventListener("targetAchieved", function (e) {
			////Do  Action after countdown 
			timer.stop();
			flagTimeLoop = 0;
			$(".countdown").remove();
			$(".main2").removeClass("active");
			$("#subPay").html("<p class=\"time-end btn btn-form btn-black display-4 \">Time is up.<br> Please make another request by choosing the option above or if you would like to submit a different profile click the button below.<br>  <button onClick=\"window.location.reload()\">Restart</button></p>");
			});
			
			//For Any Domain grab from url
			$DomainName = $(location).attr("protocol")+"//"+$(location).attr("hostname");
			
			//Timer script
			window.setInterval(event, 2000);
			function event() {
				if(flagTimeLoop == 1){
					$.ajax({ //order_id
						type :"GET",
						url : $DomainName + "/rocketr/getOrderDetails.php?order_id='.$result['paymentInstructions']['orderIdentifier'].'",
						//data: $data,     
						cache: false,
						success: function($data){    
							console.log($data);
							if($data == 0){
								//Keep scanning the API
							}
							if($data == 9){
								timer.stop();
								$(".countdown").remove();
								flagTimeLoop = 0;
								$("#qr-img").attr("src","/dist/img/rocketr/abort.png");
								$(".qramount").html("<div class=\"error-pay\">Sorry<br>Your payment did not process correctly.<br>You will receive an email from us soon to discuss the issue.</div>");
								$(".top-prices").html("<button class=\"ck-button-three btn-restart\" onClick=\"window.location.reload()\">Restart</button>");
								$(".qraddress").remove();
								
								//=======================
								//Send Invoice
								$.ajax({ 
									type: "POST",
									url:  $DomainName+"/rocketr/sendInvoice.php",
									data: $data,     
									cache:false,
									success: function($data){                          
										$(".qramount").append($data).show();     
									}           
								});
								window.location = "#subPay";
							}
							if($data == 3){
								timer.stop();
								$(".countdown").remove();
								flagTimeLoop = 0;
								$("#qr-img").attr("src","/dist/img/rocketr/paid.png");
								$(".qramount").html("Thank you<br>Your payment was completed<br>You will receive a confirmation email soon.");
								$(".top-prices").html("<button class=\"ck-button-three btn-restart\" id=\"restart\">Restart</button>");
								localStorage.setItem("ReferenceBlock", $( "#ReferenceBlock" ).html());
								localStorage.setItem("TicketPricesBlock", $( ".top-prices" ).html());
								localStorage.setItem("SubPricesBlock", $( "#subChoice .step-prices" ).html());
								$(".qraddress").remove();
								
								//=======================
								//Send Invoice
								$.ajax({ 
									type: "POST",
									url:  $DomainName+"/rocketr/sendInvoice.php",
									data: $data,     
									cache:false,
									success: function($data){                          
										$(".qramount").append($data).show();
										window.history.pushState("", "Coingeek Conference | Thank You", "/#ThankYou");
									}           
								});
								// window.location = "#subPay";
								// window.history.pushState("", "Coingeek Conference | Thank You", "/#ThankYou");
								// document.title = "Coingeek Conference | Thank You";
								setTimeout(function(){
									window.location = "/thankyou/#ThankYou";
								}, 1000);

						}           
					});
				
				}
			}
			//Stop time to fix interaction on click 
			$(".main1,.main2.active").on("click",function(e){
			    e.stopPropagation(); 
				timer.stop();
				//alert("timer stop");
			});
			
			</script>
			';
			
			echo $status;
		}
	}		
?>