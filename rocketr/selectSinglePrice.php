<?php
   	/* ====================================================================*\
		*  (c) Copyright 2017 Francesco Sorrentino. All rights reserved. *
		*  License & Distribution of the same is forbidden.              *
		*  https://sorrentino.ga                                         *
	\* ====================================================================*/
	//===================================================================================
	//Set Vars from Form Sessions
	session_start();
	
	//-----------------------------------------------------------------------------------
	//Set Vars from Form Sessions
	$oneDayPrice 	= $_SESSION['price']['oneDayPrice'];
	
	//print_r($_SESSION);
	//===================================================================================
	
	
if($_POST['selectSinglePrice']):?>
    <hr>
	<div class="step-prices">
		<h3 class="step-prices-t" >
			<div style="padding: 5px;">Choose Dates</div>
		</h3>
		<form id="singleDayPrice"  action="rocketr/createOrder.php" method="POST" enctype="multipart/form-data">
			<input name="singleDayPrice" type="hidden" value="single" />
			<input class="day1" name="Day_1" type="hidden" value="" />
			<input class="day2" name="Day_2" type="hidden" value="" />
			<input class="day3" name="Day_3" type="hidden" value="" />
			
			<div class="tiers tier2">
				<div class="pointer">
					<div style="">APPLICATION DAY</div>	
				</div>
				<div class="day-tag">DAY 1</div>
				<br>
				<div class="price-label"> 28th<br>
					<?php echo $oneDayPrice; ?> <i class="currency-box">GBP</i>
				</div>
			</div>
			<div class="tiers tier2">
				<div class="pointer">
					<div style="">APPLICATION MERCHANTS DAY</div>
				</div>
				<div class="day-tag">DAY 2</div>
				<br>
				<div class="price-label"> 29th<br>
					<?php echo $oneDayPrice; ?> <i class="currency-box">GBP</i>
				</div>
			</div>
			<div class="tiers tier3">
				<div class="pointer">
					<div style="">FUTURE PROSPECTS DAY</div>
				</div>
				<div class="day-tag">DAY 3</div>
				<br>
				<div class="price-label"> 30th<br>
					<?php echo $oneDayPrice; ?> <i class="currency-box">GBP</i>
				</div>
			</div>
			<br>
			<div class="uk-text-center uk-margin-top">
				<span class="input-group-btn align-center"><button href="" type="submit" class="btn btn-form btn-black display-4">Confirm</button></span>
			</div>
			<br>
		</form>
	</div>
	<script>
		// Step 2 Choose Tier / On click submit
		$("#singleDayPrice").on("submit",function(e){
			e.preventDefault();
			var $action = $(this).prop("action");
			var $data = $(this).serialize();
			var $this = $(this);
			$.ajax({ //Seconds Request
				type :"POST",
				url : $action,
				data: $data,     
				cache: false,
				success: function($data){                          
					$("#subPay").html($data).show();
				}           
			});
		});
		
		$("#subChoice .tiers").on("click",function(){
			var $this = $(this);
			$this.toggleClass('active');
			//subchoice
			if($(".tier1").hasClass("active")){

				$(".day1").val("selected");
			}
			else{$(".day1").val("");}
			
			if($(".tier2").hasClass("active")){
				$(".day2").val("selected");
			}
			else{$("input.day2").val("");}
			
			if($(".tier3").hasClass("active")){
				$(".day3").val("selected");
			}
			else{$("input.day3").val("");}
			
		});
		window.location = "#subChoice";
	</script>
<?php endif;?>	