<h2 class="panel__header__title" style="color: #fff; font-size: 50px;">
	<p style="text-align: center;">Speakers</p>
</h2>

<!-- content to be added here -->

<div id="speakers" class="section1 section--one-column">
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1 col-lg-12">
			<div class="col col-lg-3">
				<img src="/speakers/assets/ryan-charles.png">
				<h4>Ryan Charles – Money Button/Yours</h4>
				<p class="desc sidebar-box">Ryan X. Charles is the founder and CEO of Money Button, the world's simplest payment system for websites and apps. He is also the cofounder and CEO of Yours, a social network based on Bitcoin Cash. Formerly he was an engineer at BitGo, reddit, and BitPay. He has been programming Bitcoin wallets full-time since 2013 and has been involved in the Bitcoin industry as an enthusiast since 2011.</p>
			</div>
			<div class="col col-lg-3">
				<img src="/speakers/assets/speakers-Craig-L.jpg">
				<h4>Dr Craig Wright - Chief Scientist nChain Group</h4>
				<p class="desc sidebar-box">Dr. Wright is an Australian computer scientist, businessman, and inventor who challenges the world with visionary ideas. Currently, he is Chief Scientist for nChain – the global leader in research and development of blockchain technologies.</p>
			</div>
			<div class="col col-lg-3">
				<img src="/speakers/assets/speakers-Jimmy-L.jpg">
				<h4>Jimmy Nguyen - CEO nChain Group</h4>
				<p class="desc sidebar-box">Jimmy Nguyen is the CEO of nChain Group, the global leader in research and development of blockchain technologies – focusing on Bitcoin Cash as the true Bitcoin.</p>
			</div>
			<div class="col col-lg-3">
				<img src="/speakers/assets/elizabeth-white.jpg">
				<h4>Elizabeth White – The White Company </h4>
				<p class="desc sidebar-box">Ms. White is the CEO of The White Company, which is using blockchain technology to connect the world of global payments and financial transactions. It is unique in the industry as it offers a complete solutions package combing stablecoins, a fiat on/off ramp, a cryptocurrency exchange, wallet, merchant processing product and asset management. Ms White has worked with luxury brands around the world from the LVMH group to Formula 1 and McLaren Automotive for over 12 years. She is a “White Knight” in the crypto space focused on serving as an advocate for greater trust within the crypto community-at-large. She is also a member of The Global Legal Blockchain Consortium and a Money 20/20 Rise Up mentor. She is a graduate of NYU Stern School of Business. She is the founder of the automotive social media community @ItsWhiteNoise, drives in rally events including the Gumball 3000 and The Mille Miglia.</p>
			</div>
		</div>
		<div class="section__body__article section__body__article--id-1 col-lg-12">
			<div class="col col-lg-3">
				<img src="/speakers/assets/widya-salim.jpg">
				<h4>Widya Salim – Cryptartica</h4>
				<p class="desc sidebar-box">Widya is the co-founder of Cryptartica, the first cryptocurrency-based design platform. Cryptartica aims to lower the barrier of entry to cryptocurrency by allowing anyone to earn Bitcoin Cash through design. Having worked in 4 countries and in 2 of world’s top universities, Widya is passionate about arts, economy, cryptocurrency, and technology — all of which led to the creation of Cryptartica.</p>
			</div>
			<div class="col col-lg-3">
				<img src="/speakers/assets/michael-hudson.jpg">
				<h4>Michael Hudson – Bitstocks</h4>
				<p class="desc sidebar-box">Michael has enjoyed a successful career spanning both the technological and financial fields. This - combined with his deep passion for cryptocurrencies, politics, modern money mechanics and an inherent entrepreneurial spirit - culminated in the incorporation of Bitstocks in early 2014. He’s been featured in Thomson Reuters' Framework, The Financial Times, CityAM, The Yacht Investor, SquareMile as well as on IGTV UK, The London Square’s Shareviews, The Financial Fox and was invited to open a House of Parliament panel discussion on the Future of Finance / Blockchain. As a prolific speaker, Michael regularly attends investor events and conferences to share his extensive knowledge on cryptocurrency. His philosophical, often controversial, views have established him as a sought-after thought-leader in the cryptocurrency space, an industry he firmly believes is one destined to change the world as we know it.</p>
			</div>
			<div class="col col-lg-3">
				<img src="/speakers/assets/lorien-gamaroff.jpg">
				<h4>Lorien Gamaroff - Centbee</h4>
				<p class="desc sidebar-box">Lorien Gamaroff is the founder/CEO of Bankymoon, a blockchain and cryptocurrency consultancy and co-founder/CEO of Centbee, a cryptocurrency payments and remittance company. As South Africa's foremost blockchain expert, Lorien has been invited around the world to speak on digital/cryptocurrencies and decentralised/distributed ledgers and their benefits for emerging economies. He has addressed and advised the United Nations, IMF, World Bank, FBI and Commonwealth Secretariat, the South African Reserve Bank, Reserve Bank of Zimbabwe, TEDx and a host of professionals in multiple industries and attorney generals throughout the world. He offers insight and guidance to business executives and advises government on blockchain technologies and their implications. He pioneered blockchain payments for utility smart metering and was the co-founder of the Blockchain Academy which educates professionals on all aspects of blockchains from regulations to software development.</p>	
			</div>
			<div class="col col-lg-3">
				<img src="/speakers/assets/alex-agut.jpg">
				<h4>Alex Agut – HandCash</h4>
				<p class="desc sidebar-box">Marketer and Product Designer. Co-founder and CEO of HandCash Labs, S.L. a company focused on creating better standards and business models using Bitcoin (BCH).</p>
			</div>
		</div>
	</div>
</div>