function getMeSomeTags(title,contentArray,existing){text=title;min=1;if(contentArray.length)
{for(z=0;z<contentArray.length;z++)
{text=text+" "+contentArray[z];}
if(text.length<4000){min=2;}else{min=3;}
text=text+" "+title;}
text=text.toLowerCase();text=text.replace(/<(?:.|\n)*?>/gm,'');var stopWords=["quot","ltd","amp","nbsp","a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be","because","been","but","by","can","cannot","could","dear","did","do","does","either","else","ever","every","for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","if","in","into","is","it","its","just","least","let","like","likely","may","me","might","most","must","my","neither","no","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she","should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet","you","your","ain't","aren't","can't","could've","couldn't","didn't","doesn't","don't","hasn't","he'd","he'll","he's","how'd","how'll","how's","i'd","i'll","i'm","i've","isn't","it's","might've","mightn't","must've","mustn't","shan't","she'd","she'll","she's","should've","shouldn't","that'll","that's","there's","they'd","they'll","they're","they've","wasn't","we'd","we'll","we're","weren't","what'd","what's","when'd","when'll","when's","where'd","where'll","where's","who'd","who'll","who's","why'd","why'll","why's","won't","would've","wouldn't","you'd","you'll","you're","you've"];var re=new RegExp('\\b('+stopWords.join('|')+')\\b','g');text=(text||'').replace(re,'').replace(/[ ]{2,}/,' ');text=text.replace(/[^\w\s]|_/g,"").replace(/\s+/g," ");var atLeast=min;var numWords=0;var i,j,k,m,textlen,len,s;var keys=[null];var results=[];numWords++;for(i=1;i<=numWords;i++)
{keys.push({});}
text=text.split(/\s+/);for(i=0,textlen=text.length;i<textlen;i++)
{s=text[i];keys[1][s]=(keys[1][s]||0)+1;for(j=2;j<=numWords;j++)
{if(i+j<=textlen){s+=" "+text[i+j-1];keys[j][s]=(keys[j][s]||0)+1;}else break;}}
for(var k=1;k<=numWords;k++)
{var key=keys[k];for(var i in key)
{if(key[i]>=atLeast)results.push({word:i,times:key[i]});}}
results.sort(SortByTimes);results=results.slice(0,21);var cleanResults=[];for(var m=0;m<results.length;m++)
{cleanResults.push(results[m].word);}
var array_exist=existing.split(',');cleanResults=cleanResults.concat(array_exist.filter(function(item){return cleanResults.indexOf(item)<0;}));cleanResults=cleanResults.filter(function(e){return e});cleanResults.sort();return cleanResults}
function SortByTimes(a,b){var x=a.times;var y=b.times;return((x>y)?-1:((x<y)?1:0));}
var _paq=_paq||[];$(document).on('click','.js-tab-toggle',function(){_paq.push(['trackEvent','tab',$(this).text().trim()]);});$(document).on('click','form #vCalSubmitButton',function(){var formObj=$(this).closest('form');var _piwikAction=formObj.find('#name').val();var _piwikName=formObj.find('#startTime').val()+' - '+formObj.find('#endTime').val();_paq.push(['trackEvent','vcal',_piwikAction,_piwikName]);});