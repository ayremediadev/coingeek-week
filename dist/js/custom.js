//============================================================
// Step 1 Contact Form
//============================================================
$('#form-step-1').on('submit',function(e){
	
	e.preventDefault();
	
	var $action = $(this).prop('action');
	var $data = $(this).serialize();
	var $this = $(this);
	
	$this.prevAll('.alert').remove();
	
	$.post( $action, $data, function( data ) {
		
		if( data.response=='error' ){
			$this.before( '<div class="alert alert-danger"><i class="fa fa-times-circle alert-close"></i>'+data.message+'</div>' );
			window.location = '#form-rocketr';
			history.replaceState(null, null, ' ');
		}
		
		if( data.response=='success' ){
			$this.before( '<div class="alert alert-success"><i class="fa fa-times-circle alert-close"></i>'+data.message+'</div>' );
			$this.find('input, textarea').val('');
			$('#form-rocketr').html('<div class="final-msg"><strong>Thank You!</strong> You have succesfully bought a ticket.</div>');
			window.location = '#form-rocketr';
			history.replaceState(null, null, ' ');
		}		
		
		if( data.response=='selectPriceType' ){
			$('#form-rocketr').html(data.message);
			window.location = '#form-rocketr';
			history.replaceState(null, null, ' ');
		}
		
		if( data.response=='standard' ){
			$this.before( '<div class="alert alert-standard"><i class="fa fa-times-circle alert-close"></i>'+data.message+'</div>' );
			$this.find('input, textarea').val('');
			$('form-rocketr').html('<div class="final-msg">You have sent us an enquiry.</div>');
			window.location = '#form-rocketr';
			history.replaceState(null, null, ' ');
		}
		
	}, "json");
});

//Remove alerts 
$('input').on('click',function(){
	$('.alert').remove();
});
$(function() {
	$('body').on('click', '.alert-close', function() {
		$('.alert').remove();
	});
});

//============================================================
// Animated scrolling / Scroll Up
//============================================================

(function () {
	$('a[href*="#"]').bind("click", function(e){
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top
		}, 1000);
		e.preventDefault();
		history.replaceState(null, null, ' ');
	});
}());


// var swiper = new Swiper('.swiper-container', {
// spaceBetween: 30,
// centeredSlides: true,
// autoplay: {
// delay: 2500,
// disableOnInteraction: false,
// },
// pagination: {
// el: '.swiper-pagination',
// clickable: true,
// },
// navigation: {
// nextEl: '.swiper-button-next',
// prevEl: '.swiper-button-prev',
// },
// });

var swiper = new Swiper('.swiper-headliners', {
	slidesPerView: 4,
	spaceBetween: 30,
	//slidesPerGroup: 3,
	loop: false,
	loopFillGroupWithBlank: true,
	autoplay: {
        delay: 2500,
        disableOnInteraction: false,
	},
	pagination: {
        el: '.swiper-pagination',
        clickable: true,
	},
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
	},
	breakpoints: {
        1024: {
			slidesPerView: 3,
			spaceBetween: 40,
		},
        768: {
			slidesPerView: 2,
			spaceBetween: 30,
		},
        645: {
			slidesPerView: 2,
			spaceBetween: 20,
		},
        580: {
			slidesPerView: 1,
			spaceBetween: 10,
		}
	}
});

var swiper = new Swiper('.swiper-sponsors', {
	slidesPerView: 4,
	spaceBetween: 30,
	//slidesPerGroup: 3,
	loop: false,
	loopFillGroupWithBlank: true,
	autoplay: {
        delay: 2500,
        disableOnInteraction: false,
	},
	breakpoints: {
        1024: {
			slidesPerView: 3,
			spaceBetween: 40,
		},
        768: {
			slidesPerView: 2,
			spaceBetween: 30,
		},
        645: {
			slidesPerView: 2,
			spaceBetween: 20,
		},
        580: {
			slidesPerView: 1,
			spaceBetween: 10,
		}
	}
});
//======================================================
//==  Menu
//======================================================	


//======================================================
//==  Menu icon
//======================================================
$('document').ready(function () {
    var trigger = $('#hamburger'),
	isClosed = true;
	
    trigger.click(function () {
		burgerTime();
	});
	
    function burgerTime() {
		if (trigger.hasClass('is-closed')) {
			trigger.removeClass('is-closed');
			trigger.addClass('is-open');
			isClosed = true;
			openNav();
			} else {
			trigger.removeClass('is-open');
			trigger.addClass('is-closed');
			isClosed = false;
			closeNav()
		}
	}
	
	function openNav() {
		document.getElementById("mySidenav").style.width = "300px";
	}
	
	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
	}
	
	$("#mySidenav a").click(function(){
		burgerTime();
	});
	
	$(".site").click(function(){
		if($('#hamburger').hasClass('is-open')){
			burgerTime();
		}
	});
	
});

//======================================================
//==  Testimonial slider
//======================================================
$(document).ready(function(){
	$(".w-testimonials--id-uid_A9C94 .w-testimonials__list").bxSlider({
		speed:1000
		, mode:"fade"
		, randomStart:false
		, auto: true
		, controls: true
		, pager: true
		, captions: true
	});
});

//======================================================
//==  speakers headers motion
//======================================================
//$('document').ready(function () {

//$(".reveal .panel--slide-up").hover(function(){
//$(".m-speakers-list__items__item__header-body", this).style.bottom = "0";
//$(".panel--slide-up .panel__body", this).show();
//});

//});


// var timerPlan1 = new Timer();
// timerPlan1.start({countdown: true, startValues: {minutes:0, seconds: 30}});
// $(".countdown_ticket1").html(timerPlan1.getTotalTimeValues().minutes+":"+timerPlan1.getTotalTimeValues().seconds);

// timerPlan1.addEventListener("secondsUpdated", function (e) {
// $(".countdown_ticket1").html(timerPlan1.getTimeValues().minutes+":"+timerPlan1.getTimeValues().seconds);
// });
// timerPlan1.addEventListener("targetAchieved", function (e) {
//Do  Action after countdown 
// $(".timer1").remove();
// $(".plan1").removeClass("active");
// $(".plan2").addClass("active");
// $(".timer2").html("<b class='countdown_ticket2'></b><br>Until the Late Bird Plan Expaires.");

//////change tier name to forward on submit 
// $("#tierType").val("tier2");

//////------------------------------------------------------------
// var timerPlan2 = new Timer();
// timerPlan2.start({countdown: true, startValues: {days:20, hours: 24, minutes:59, seconds: 30}});
// $(".countdown_ticket2").html(timerPlan2.getTotalTimeValues().days+":"+timerPlan2.getTotalTimeValues().hours+":"+timerPlan2.getTotalTimeValues().minutes+":"+timerPlan2.getTotalTimeValues().seconds);

// timerPlan2.addEventListener("secondsUpdated", function (e) {
// $(".countdown_ticket2").html(timerPlan2.getTimeValues().minutes+":"+timerPlan2.getTimeValues().minutes+":"+timerPlan2.getTimeValues().minutes+":"+timerPlan2.getTimeValues().seconds);
// });
// timerPlan2.addEventListener("targetAchieved", function (e) {
//Do  Action after countdown 
// $(".timer2").remove();
// $(".plan2").removeClass("active");
// $(".plan3").addClass("active");
// $(".timer3").html("<b class='countdown_ticket3'>Standard Plan Only</b>");

/////change tier name to forward on submit 
// $("#tierType").val("tier3");
// });
//////------------------------------------------------------------
// });

$('#tickets td').on( 'click', function(){
	window.location = '#buy-pass';
});

//PopUp
$('.profile-sngl').click( function (){
	$('.profile-box').removeClass('active');
	$(this).find('.profile-box').addClass('active');
});

//close PopUp
$('.profile-close').on('click', function (){
	$('.profile-box').removeClass('active');
});

$(function() {
	$('body').on('click', '.profile-close', function(e) {
		e.preventDefault();
		$('.profile-box').removeClass('active');
	});
});


//=========================================================
//Remove Uri
history.replaceState(null, null, ' ');
