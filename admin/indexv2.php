<!DOCTYPE html>
<?php
	/* ==================================================================== *\
		*  (c) Copyright 2017 Francesco Sorrentino. All rights reserved. *
		*  License & Distribution of the same is forbidden.              *
		*  https://sorrentino.ga                                         *
	\* ==================================================================== */
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	$servername = "localhost:3306";
	$username = "coingeek_w_admin";
	$password = "O#lB}qpICn}F";
	$dbname = "coingeek_week_temp";
	
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	$sql = "SELECT id, fullname, company, email, phone, message, categories, bcomm_assoc_status, subscribe, industry, tierTitle, refNumb, ticket, price, payment_status, date FROM attendees";
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
		// output data of each row
		while($rows = $result->fetch_assoc()) {
			foreach($rows as $row){}
			$id         		= $rows['id'];
			//$subject    		= $rows['subject'];
			$fullname   		= $rows['fullname'];
			$email     			= $rows['email'];
			$phone      		= $rows['phone'];
			$company   		 	= $rows['company'];
			$message    		= $rows['message'];
			$categories 		= $rows['categories'];
			$bcomm_assoc_status = $rows['bcomm_assoc_status'];
			$subscribe  		= $rows['subscribe'];
			$industry  			= $rows['industry'];
			$tierTitle  		= $rows['tierTitle'];
			$refNumb  			= $rows['refNumb'];
			$ticket 			= $rows['ticket'];
			$price  			= $rows['price'];
			$payment_status   	= $rows['payment_status'];
			$date  				= $rows['date'];
			
			//================================================================================
			//Table View
			
			$table[] ='	
		<tr>
		<td class="noExl noSec"><input type="checkbox" id="row_'.$id .'" name="row_'.$id .'" /></td>
		<td class="noSec">'.$id .'</td>
		<td class="noSec">'.$fullname.'</td>
		<td class="noSec">'.$email.'</td>
		<td class="noSec">'.$phone.'</td>
		<td class="noSec">'.$company.'</td>
		<td class="noSec">'.$message.'</td>
		<td class="noSec">'.$categories.'</td>
		<td class="noSec">'.$bcomm_assoc_status.'</td>
		<td class="noSec">'.$subscribe.'</td>
		<td class="noSec">'.$industry .'</td>
		<td class="noSec">'.$tierTitle.'</td>
		<td class="noSec">'.$refNumb .'</td>
		<td class="noSec">'.$ticket.'</td>
		<td class="noSec">'.$price.'</td>
		<td class="noSec">'.$payment_status.'</td>
		<td class="noSec">'.$date.'</td>
		';
		
		$table[] .='
		</tr>
		';
		
		}
		
		} else {
		$table[] = null;
		}
		$conn->close();	
		
		
		?>
		
		
		<html>
		<head>
		<!-- Start META Tags -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta http-equiv="expires" content="timestamp">
		
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, minimal-ui"/> <!--320-->
		<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon" />
		
	    <link rel="stylesheet" type="text/css" href="//akottr.github.io/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="//akottr.github.io/css/akottr.css" />
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/dragtable.css" />
		
		
		<style>
		.tools-bar{
		background:#000000;
		border-radius:5px;
		padding:5px;
		margin:5px 0;
		}
		.table-content {
		box-shadow: 0px 0px 19px;
		padding: 10px;
		}
		th hr {
		margin: 5px;
		border: 1px solid #444;
		}
		.row{overflow:auto;}
		</style>
		
		
		
		</head>
		<body>
		<h1 class="page-title"><b>Archive</b></h1>
		
		<div class="cont">
		<div class="box">
		<div class="box-body">
		<br>
		
		</div>
		</div>
		<div class="box">
		<h2 class="overview-t">Conference Attendees</h2>
		<div class="box-body">
		<br>
		<div class="tools-bar">
		<button id="btn-export-all" class="btn btn-primary">Export All</button>
		<button id="btn-export-sel" class="btn btn-primary">Export Selected</button>
		</div>
		<div class="table-content">
		<table id="archiveTable" class="defaultTable sar-table table table-striped table-bordered" style="width:100%">
		<thead>
		<tr>
		<th class="noExl noSec"><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		<th class="noExl accept noSec"><hr><hr></th>
		</tr>
		<tr>
		<th class="noExl noSec"><input type="checkbox" id="checkAll" name="checkAll" /></th>
		<th class="noExl  noSec">ID</th>
		<th class="noExl  noSec">Full Name</th>
		<th class="noExl  noSec">Email</th>
		<th class="noExl  noSec">Phone</th>
		<th class="noExl  noSec">Company Name</th>
		<th class="noExl  noSec">Message</th>
		<th class="noExl  noSec">Categories</th>
		<th class="noExl  noSec">bComm Status</th>
		<th class="noExl  noSec">Subscribe Status</th>
		<th class="noExl  noSec">Industry</th>
		<th class="noExl  noSec">Tier Title</th>
		<th class="noExl  noSec">Ref. NUMBER</th>
		<th class="noExl  noSec">Ticket</th>
		<th class="noExl  noSec">Price</th>
		<th class="noExl  noSec">Payment Status</th>
		<th class="noExl  noSec">Date</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($table as $row){
		echo $row;
		}?>
		</tbody>
		<tfoot>
		<tr>
		<th class="noExl noSec"><input type="checkbox" id="checkAll" name="checkAll" /></th>
		<th class="noExl accept noSec">ID</th>
		<th class="noExl accept noSec">Full Name</th>
		<th class="noExl accept noSec">Email</th>
		<th class="noExl accept noSec">Phone</th>
		<th class="noExl accept noSec">Company Name</th>
		<th class="noExl accept noSec">Message</th>
		<th class="noExl accept noSec">Categories</th>
		<th class="noExl accept noSec">Join Status</th>
		<th class="noExl accept noSec">Subscribe Status</th>
		<th class="noExl accept noSec">Industry</th>
		<th class="noExl accept noSec">Tier Title</th>
		<th class="noExl accept noSec">Ref. NUMBER</th>
		<th class="noExl accept noSec">Ticket</th>
		<th class="noExl accept noSec">Price</th>
		<th class="noExl accept noSec">Payment Status</th>
		<th class="noExl accept noSec">Date</th>
		</tr>
		</tfoot>
		</table>
		</div>
		<div class="tools-bar">
		
		</div>
		</div>
		</div>
		</div>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
		
		<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js "></script>
		
		<script src="assets/js/jquery.dragtable.js"></script>
		<script src="assets/js/jquery.table2excel.min.js"></script>
		
		
		<script type="text/javascript">
		
		$(document).ready(function() {     
		//Full Table
		$('.defaultTable').DataTable();
		
		//Drag
		$('.defaultTable').dragtable({dragaccept:'.accept'});
		
		//Download/Export table
		$("#btn-export-all").click(function(){
		$(".defaultTable").table2excel({
		exclude: ".noExl",
		name: "Excel Document Name",
		filename: "Full_Archive_Bccom",
		fileext: ".xls",
		exclude_img: true,
		exclude_links: true,
		exclude_inputs: true
		});
		});
		
		$("#btn-export-sel").click(function(){
		$(".defaultTable").table2excel({
		exclude: ".noSec",
		exclude: ".noExl",
		name: "Excel Document Name",
		filename: "Selection_Archive_Bccom",
		fileext: ".xls",
		exclude_img: true,
		exclude_links: true,
		exclude_inputs: true
		});
		});
		//Checkboxes
		$('input:checkbox').change(function(){
		var target = $("tr > td");
		if($(this).is(":checked")) {
		//alert('checked');
		
		this.target.closest().removeClass("noSec");
		
		} else {
		//alert('no unchecked');
		this.target.closest().addClass("noSec");
		}
		});
		
		});
		
		</script>
		</body>
		</html>		