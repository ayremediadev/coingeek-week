<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<?php
	session_start();
	//print_r($_SESSION);
	session_destroy();//clear past sessions on refresh.
?>
<!DOCTYPE html>
<html lang="en-GB" class="no-js">
	<head>
		<script src="https://coingeekweek.com/jquery-3.3.1.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Coingeek cryptocurrency and blockchain conference | London 27th-30th November 2018</title>
		
		<!-- BOOTSTRAP -->
        <!-- Latest compiled and minified CSS -->
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
        <!-- Optional theme -->
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">-->
        <!-- Latest compiled and minified JavaScript -->
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>-->

		<!-- Meta description general -->
		<meta name="robots" 	content="index,follow" />
		<meta name="author" 	content="">
		<meta name="description"content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<meta name="keywords" 	content="CoinGeek Week Conference, Marketing Festival, Marketing event, Marketers, Marketing conference, Brand companies, Marketing consultancy" />
		<meta name="viewport" 	content="width=device-width, initial-scale=1.0" />
		<!-- Meta description -->
		<meta property="og:image" 		content="https://coingeekweek.com/dist/img/social_logo.jpg" />
		<meta property="og:type" 		content="website" />
		<meta property="og:site_name" 	content="Coingeek Conference" />
		<meta property="og:title" 		content="Coingeek cryptocurrency and blockchain conference | London 28th-30th November 2018" />
		<meta property="og:description" content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<meta property="schema:name" typeof="http://schema.org/WebPage" 		content="Coingeek Conference" />
		<meta property="schema:image" typeof="http://schema.org/WebPage" 		content="https://coingeekweek.com/dist/img/social_logo.jpg" />
		<meta property="schema:description" typeof="http://schema.org/WebPage" 	content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<!-- Meta Twitter -->
		<meta name="twitter:card" content="summary_large_image">
		
		<!-- Favicons -->
		<link rel="shortcut icon" href="../dist/img/ico/favicon.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" 	href="/dist/img/ico/144.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" 	href="/dist/img/ico/114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" 		href="/dist/img/ico/72.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" 		href="/dist/img/ico/57.png">

		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
		<link href="custom.css" rel="stylesheet" type="text/css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

		<link rel="stylesheet" href="../dist/css/global.min.css">


		<!-- Theme CSS -->
		<link href="//fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet">
		<link rel="stylesheet" href="https://use.typekit.net/bea6eml.css">

	<script>
		<!--[if IE]>
			<style>
			.path-rotation::before{
			opacity: 0;
			}
			</style>
		<![endif]-->
		
		<!-- Support for Media Queries in IE8 -->
		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1-1-0/respond.min.js"></script>
		<![endif]-->
		
		<!--\ Google Captcha/-->
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		
		<!-- Google Tag Manager -->
		
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		
		})(window,document,'script','dataLayer','GTM-M9WBCPN');</script>
		
		<!-- End Google Tag Manager -->

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '2100522493292304');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=2100522493292304&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
		<style>
			@media only screen and (min-width: 800px) {
				.sponsors .panel, .sponsors .panel:first-child {
					width: 20%;
					padding: 0;
				} 		
				.sponsors.powered.section__body {
					max-width: 90%; 
					margin: auto; 
					padding-bottom: 0;
				}
				.article .article__header .article__header__title.after_party {
					margin-top: 1em; 
				}
				p {
					margin: 0 0 1em 0;
					font-weight: 500 !important;
				}
				.sponsors .panel img.first {
					width: 100% !important;
				}
				.sponsors .panel img {
					filter: none;
					width: 50% !important;
					opacity: 1;
				}
			}
		</style>

	</head>
	<body class="body--id-9692 t-homepage FOM-2018">
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9WBCPN"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<header class="header">
			<?php include_once '../part/nav-mobile-sub.php'; ?>
			<?php include_once '../part/nav-sub.php'; ?>		
		</header>
		<div class="site">
<style>
	#contact-form{

	}
	#contact-form .form-control{
		border-radius: 0px;
		border: 0px;
	}
	#contact-form .g-recaptcha div{
		margin: 0px auto;
	}
	#contact-form .form-group{
		margin-bottom: 0px;
		color: #fff;
	}
	#contact-form .form-group label{
		color: #fff;
	}
	.menu__item a:hover{
		text-decoration: none;
	}

	.help-block.with-errors{
		position: absolute;
		top: 5px;
		right: 5px;
		font-size: 0.7rem;
	}
	.recaptcha-wrap .help-block.with-errors{
		position: relative;
	}
	.help-block.with-errors ul{
		margin: 0px;
	}
</style>

<div id="buy-pass" class="cta">
<div class="panel panel--default panel--id-9857">


<?php /******
 * 
 * 
 * CONTENT TO START BELOW HERE
 * IN THE "by-pass" div.
 * 
 * 
 *  ******/


include_once 'content.php';



/******
 * 
 * 
 * CONTENT TO START BELOW HERE
 * IN THE "by-pass" div.
 * 
 * 
 *  ******/
?>

</div>
</div>




			<?php //include_once 'part/poweredby.php';?>
			
			
			<?php include_once '../part/footer.php'; ?>
		</div>
		<!-- JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script src="validator.js"></script>
		<script src="contact.js"></script>
		<script type="text/javascript" src="dist/js/script.min.js"></script>
		
		<script src="../dist/js/showoff.global.js" ></script>
		
		<!-- Swiper JS -->
		<script src="../dist/js/swiper.min.js"></script>
		<!-- Custom JS -->
		<script src="../dist/js/custom.js"></script>
		<!-- Parserly JS -->

		<!-- IE CSS FIX -->
		<script>
			var isIE = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
			if(isIE){
				//document.getElementsByClassName("path-rotation").style.opacity = "0";
				//var x = document.getElementsByClassName("path-rotation");
				//x[0].style.opacity = "0";
				$(".path-rotation").attr('style',  'opacity:0!important');
				//alert('internet explorer');
			}
		</script>
		<!-- Start of LiveChat (www.livechatinc.com) code -->
		<script type="text/javascript">
			window.__lc = window.__lc || {};
			window.__lc.license = 9956175;
			(function() {
				var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
				lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
			})();
		</script>
		<!-- End of LiveChat code -->
	</body>
</html>									