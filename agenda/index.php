<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<?php
	session_start();
	//print_r($_SESSION);
	session_destroy();//clear past sessions on refresh.
?>
<!DOCTYPE html>
<html lang="en-GB" class="no-js">
	<head>
		<script src="https://coingeekweek.com/jquery-3.3.1.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Coingeek cryptocurrency and blockchain conference | London 27th-30th November 2018</title>
		
		
		<!-- Meta description general -->
		<meta name="robots" 	content="index,follow" />
		<meta name="author" 	content="">
		<meta name="description"content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<meta name="keywords" 	content="CoinGeek Week Conference, Marketing Festival, Marketing event, Marketers, Marketing conference, Brand companies, Marketing consultancy" />
		<meta name="viewport" 	content="width=device-width, initial-scale=1.0" />
		<!-- Meta description -->
		<meta property="og:image" 		content="https://coingeekweek.com/dist/img/social_logo.jpg" />
		<meta property="og:type" 		content="website" />
		<meta property="og:site_name" 	content="Coingeek Conference" />
		<meta property="og:title" 		content="Coingeek cryptocurrency and blockchain conference | London 28th-30th November 2018" />
		<meta property="og:description" content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<meta property="schema:name" typeof="http://schema.org/WebPage" 		content="Coingeek Conference" />
		<meta property="schema:image" typeof="http://schema.org/WebPage" 		content="https://coingeekweek.com/dist/img/social_logo.jpg" />
		<meta property="schema:description" typeof="http://schema.org/WebPage" 	content="Get involved with bitcoin talks and debates with expert speakers like Craig Wright this November 28th – 30th in Central London at the Coingeek cryptocurrency and blockchain conference" />
		<!-- Meta Twitter -->
		<meta name="twitter:card" content="summary_large_image">
		
		<!-- Favicons -->
		<link rel="shortcut icon" href="../dist/img/ico/favicon.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" 	href="/dist/img/ico/144.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" 	href="/dist/img/ico/114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" 		href="/dist/img/ico/72.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" 		href="/dist/img/ico/57.png">

		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
		<link href="custom.css" rel="stylesheet" type="text/css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

		<link rel="stylesheet" href="../dist/css/global.min.css">


		<!-- Theme CSS -->
		<link href="//fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet">
		<link rel="stylesheet" href="https://use.typekit.net/bea6eml.css">

	<script>
		<!--[if IE]>
			<style>
			.path-rotation::before{
			opacity: 0;
			}
			</style>
		<![endif]-->
		
		<!-- Support for Media Queries in IE8 -->
		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1-1-0/respond.min.js"></script>
		<![endif]-->
		
		<!--\ Google Captcha/-->
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		
		<!-- Google Tag Manager -->
		
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		
		})(window,document,'script','dataLayer','GTM-M9WBCPN');</script>
		
		<!-- End Google Tag Manager -->

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '2100522493292304');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=2100522493292304&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
		<style>
			@media only screen and (min-width: 800px) {
				.sponsors .panel, .sponsors .panel:first-child {
					width: 20%;
					padding: 0;
				} 		
				.sponsors.powered.section__body {
					max-width: 90%; 
					margin: auto; 
					padding-bottom: 0;
				}
				.article .article__header .article__header__title.after_party {
					margin-top: 1em; 
				}
				p {
					margin: 0 0 1em 0;
					font-weight: 500 !important;
				}
				.sponsors .panel img.first {
					width: 100% !important;
				}
				.sponsors .panel img {
					filter: none;
					width: 50% !important;
					opacity: 1;
				}
			}
		</style>

	</head>
	<body class="body--id-9692 t-homepage FOM-2018">
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9WBCPN"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<header class="header">
			<?php include_once '../part/nav-mobile-sub.php'; ?>
			<?php include_once '../part/nav-sub.php'; ?>		
		</header>
		<div class="site">
<style>
	#contact-form{

	}
	#contact-form .form-control{
		border-radius: 0px;
		border: 0px;
	}
	#contact-form .g-recaptcha div{
		margin: 0px auto;
	}
	#contact-form .form-group{
		margin-bottom: 0px;
		color: #fff;
	}
	#contact-form .form-group label{
		color: #fff;
	}
	.menu__item a:hover{
		text-decoration: none;
	}

	.help-block.with-errors{
		position: absolute;
		top: 5px;
		right: 5px;
		font-size: 0.7rem;
	}
	.recaptcha-wrap .help-block.with-errors{
		position: relative;
	}
	.help-block.with-errors ul{
		margin: 0px;
	}
</style>
<link rel="stylesheet" type="text/css" href="https://coingeekweek.com/responsive.css"/>
<div id="buy-pass" class="cta">
	<div class="panel panel--default panel--id-9857">
		<div class="panel__header">
			<h2 class="panel__header__title" style="color: #fff; font-size: 50px;">
				<p style="text-align: center;">Agenda</p>
			</h2>
		</div>
		<div class="panel__body">
			<!-- <p style="text-align: center;">(Invitation Only)</p> -->		
		</div>
		<div class="section__body" style="padding: 20px;max-width: 1000px;margin: auto; font-weight: 900; color: #fff; padding-bottom: 90px;">
			<p style="text-align: center;">Enjoy three days filled with expert talks, Q&As and networking. Click below to discover the timelines. Full agenda, including speakers, will be revealed soon.</p>
		</div>

<!-- Demo styles -->
<div id="agenda" class="section section--one-column section--id-5 section--feature" style="padding-top: 0">
	<div class="section__body">
		<div class="section__body__article section__body__article--id-1">
			<a class="anchor"></a> 
			<article class="article article--featured">
				<div class="article__image"> 
					<img src="/dist/img/general/Agenda2.jpg" /> 
				</div>
				<div class="agenda_cont">
					<!-- <div class="article__header">
						<h3 class="article__header__title"> Agenda </h3>
						<p style="color:#ffffff; margin:auto; max-width:880px; "> Enjoy three days filled with expert talks, Q&As and networking. Click below to discover the timelines. Full agenda, including speakers, will be revealed soon. </p>
						<i id="backToAgenda"></i>
					</div> -->

						<div class="accordion">
							<div class="tab">
								<input id="tab-one" type="checkbox" name="tabs">
								<label for="tab-one">28th November 2018</label>
								<div class="tab-content">
									<br>
									<h3 style="color: #000;font-size: 22px;">Application Developers Day</h3>
									<p style="color: #000; font-size: 14px;" >On Wednesday you will learn everything you need to know about the latest within blockchain technology. Learn from the industry pioneers and immerse yourself into the great opportunities blockchain technology will bring to your business. We welcome all our attendees to keep the exciting conversation going at the afternoon networking event.  
									</p>
									<table class="stack desktopAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td colspan="2">REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td>Introduction to CoinGeek Week</td>
												<td>Calvin Ayre (Ayre Media)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td><span color="#ff0000;">FREE SPACE</span></td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>The Future of Commerce</td>
												<td>James Belding (Tokenized)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>BCH & a Global Supply Chain solution in the world's largest ERP system</td>
												<td>Stephan Nilsson (Unisot)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Money Button: Easy Payments for Everybody Everywhere</td>
												<td>Ryan X. Charles</td>  
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Taking Care of Business:  Business Uses of Bitcoin</td>
												<td>Dr. Craig S. Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td colspan="2">Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Smart Contracts, Dumb Contracts - How To Build a Business on the Blockchain</td>
												<td>Kristy-Leigh Minehan</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>The New Money</td>
												<td>Alex Agut (HandCash)</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Human Readable Data in the Block Chain</td>
												<td>Erich Erstu (CryptoGraffiti.info)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30pm-3:45pm</td>
												<td>Building B2B Solutions on Bitcoin for Industry 4.0</td>
												<td>Rogelio Reyna (Binde)</td>  
											</tr>
											<tr>
												<td style="width:150px;">3:45-4:15pm</td>
												<td>Panel - What’s Next for building on Bitcoin SV Alex Agut, Handcash Jerry Chan, SBI Ryan X Charles, Money Button</td>
												<td>Jimmy Nguyen</td>
											</tr>
											<tr>
												<td style="width:150px;">4:15-4:30pm</td>
												<td>Closing remarks</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
										</tbody>
									</table>
									<table class="stack mobileAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td>REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td>Introduction to CoinGeek Week, Calvin Ayre (Ayre Media)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td><span color="#ff0000;">FREE SPACE</span>, Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>The Future of Commerce, James Belding (Tokenized)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>BCH & a Global Supply Chain solution in the world's largest ERP system, Stephan Nilsson (Unisot)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Money Button: Easy Payments for Everybody Everywhere, Ryan X. Charles</td>  
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Taking Care of Business:  Business Uses of Bitcoin, Dr. Craig S. Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td>Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Smart Contracts, Dumb Contracts - How To Build a Business on the Blockchain, Kristy-Leigh Minehan</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>The New Money, Alex Agut (HandCash)</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Human Readable Data in the Block Chain, Erich Erstu (CryptoGraffiti.info)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30pm-3:45pm</td>
												<td>Building B2B Solutions on Bitcoin for Industry 4.0, Rogelio Reyna</td>  
											</tr>
											<tr>
												<td style="width:150px;">3:45-4:15pm</td>
												<td>Panel - What’s Next for building on Bitcoin SV Alex Agut, Handcash Jerry Chan, SBI Ryan X Charles, Money Button, Jimmy Nguyen</td>
											</tr>
											<tr>
												<td style="width:150px;">4:15-4:30pm</td>
												<td>Closing remarks, Jimmy Nguyen (nChain)</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab">
								<input id="tab-two" type="checkbox" name="tabs">
								<label for="tab-two">29th November 2018</label>
								<div class="tab-content">
									<br>
									<h3 style="color: #000;font-size: 22px;">Application Merchants Day</h3>
									<p style="color: #000; font-size: 14px;" >Tune in on Thursday to get the advantage over your competitors. The leaders of the industry will enlighten you on new developments, updates and demonstrate how adopting Bitcoin Cash is the fundament for growing your business. We welcome all our attendees to keep the exciting conversation going at the afternoon networking event.
									</p>
									<table class="stack desktopAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td colspan="2">REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td></td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>Why to be passionate about Bitcoin?</td>
												<td>Daniel Lipshitz (GAP600)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Web 3.0 on Bitcoin</td>
												<td>Jason Chavannes (Memo.bch)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Beer + People = Unicorns</td>
												<td>Martin Dempster (Brewdog)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Building a Two-Sided Network</td>
												<td>Elizabeth White (The White Company)</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>The future of work, the future of tax and the future of money</td>
												<td>Dominic Frisby (Moneyweek)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td colspan="2">Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Merchant Payments and a Better User Experience with Bitcoin</td>
												<td>Lorien Gamaroff (Centbee)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Cryptocurrency Adoption for Payments</td>
												<td><span color="#ff0000;">Ina Samovich</span></td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Designing a Better World</td>
												<td>Widya Salim (Cryptartica)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:30pm</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:00pm</td>
												<td> Instant transactions for Bitcoin</td>
												<td>Steve Shadders (nChain)</td>  
											</tr>
											<tr>
												<td style="width:150px;">4:00-4:30pm</td>
												<td>Bitcoin payments in the real world</td>
												<td>Angus Brown (CentBee)</td>
											</tr>
											<tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Closing Remarks</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">4:45pm</td>
												<td colspan="2">Cocktail Reception</td>
											</tr>
										</tbody>
									</table>
									<table class="stack mobileAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td>REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9.15-9:30am</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>Why to be passionate about Bitcoin?, Daniel Lipshitz (GAP600)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Web 3.0 on Bitcoin, Jason Chavannes (Memo.bch)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Beer + People = Unicorns, Martin Dempster (Brewdog)</td>
											</tr>
											<tr>
												<td style="width:150px;">11.00-11:20am</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>Building a Two-Sided Network, Elizabeth White (The White Company)</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>The future of work, the future of tax and the future of money, Dominic Frisby (Moneyweek)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td>Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30pm-2:00pm</td>
												<td>Merchant Payments and a Better User Experience with Bitcoin, Lorien Gamaroff (Centbee)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Cryptocurrency Adoption for Payments, Ina Samovich</span></td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>Designing a Better World, Widya Salim (Cryptartica)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:30pm</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:00pm</td>
												<td>Instant transactions for Bitcoin, Steve Shadders (nChain)</td>  
											</tr>
											<tr>
												<td style="width:150px;">4:00-4:30pm</td>
												<td>Bitcoin payments in the real world, Angus Brown (CentBee)</td>
											</tr>
											<tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Closing Remarks, Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">4:45pm</td>
												<td>Cocktail Reception</td>
											</tr>
										</tbody>
									</table>									
								</div>
							</div>
							<div class="tab">
								<input id="tab-three" type="checkbox" name="tabs">
								<label for="tab-three">30th November 2018</label>
								<div class="tab-content">
									<br><h3 style="color: #000;font-size: 22px;">The Future</h3>
									<p style="color: #000; font-size: 14px;" >Friday will round off the week with keynotes from the true pioneers of blockchain. Add to everything you have learned this week by taking in what our experts are predicting in the future of Bitcoin Cash technology. After an intense week of learning, it’ll be time to relax, we invite all our ‘all-days’ ticket holders to an extravagant celebration at and exclusive venue in central London, which will be in a true Calvin Ayre style. 
									</p>
									<table class="stack desktopAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td colspan="2">REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9:10-9:30am</td>
												<td></td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>To the Moon!</td>
												<td>Jerry Chan (SBI Holdings)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Bringing the Crypto Space out of the Basement and into the Boardroom</td>
												<td>Taras Kulyk</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Dominic Frisby's Big Money Gameshow</td>
												<td>Dominic Frisby (MoneyWeek)</td>
											</tr>
											<tr>
												<td style="width:150px;">11:00-11:20am</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>In their Own Words:  Bitcoin Merchant Testimonials – Panel discussion<br/>
													Moderated by: Jimmy Nguyen<br/>BrewDog – Martin Dempster, VP of Innovation<br/>White Company – Elizabeth White, CEO<br/>Coppay - Ina Samovich, CEO<br/>CentBee – Heidi Patmore, Head of Marketing
												</td>
												<td>TBC</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Going Meta on Bitcoin</td>
												<td>Dr. Craig Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td colspan="2">Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30-2:00pm</td>
												<td>Bitcoin: Global Adoption Starts Here</td>
												<td>Kristy-Leigh Minehan (Core Scientific)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Tools of Humanity</td>
												<td>Michael Hudson, Bitstocks</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>What Role Should Exchanges Play in Bitcoin Governance? MD, Circle. Former CSO, OKEx & OKCoin</td>
												<td>Jack C. Liu (Circle)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td colspan="2">Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:30pm</td>
												<td>Growing Up: It's Time for Bitcoin and Bcommerce to Professionalize<br/>
												Moderated by: Jimmy Nguyen<br />
												Edgar Maximillian <br /> 
												Iqbal Gandham  - eToro<br />Michael Hudson
											</td>
												<td>TBC</td>  
											</tr>
											<!-- <tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Closing remarks</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:00pm-till late</td>
												<td colspan="2">Party</td>
											</tr> -->
										</tbody>
									</table>
									<table class="stack mobileAgenda">
										<tbody>
											<tr>
												<td style="width:150px;">8:00-9:00am</td>
												<td>REGISTRATION</td>
											</tr>
											<tr>
												<td style="width:150px;">9:10-9:30am</td>
												<td>Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:30-10:00am</td>
												<td>To the Moon!, Jerry Chan (SBI Holdings)</td>
											</tr>
											<tr>
												<td style="width:150px;">10:00-10:30am</td>
												<td>Bringing the Crypto Space out of the Basement and into the Boardroom, Taras Kulyk</td>
											</tr>
											<tr>
												<td style="width:150px;">10:30-11:00am</td>
												<td>Dominic Frisby's Big Money Gameshow, Dominic Frisby (MoneyWeek)</td>
											</tr>
											<tr>
												<td style="width:150px;">11:00-11:20am</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">11:30am-12.00pm</td>
												<td>In their Own Words:  BCH Merchant Testimonials – Panel discussion<br/>
													Moderated by: Jimmy Nguyen<br/>
													BrewDog – Martin Dempster, VP of Innovation<br />
													White Company – Elizabeth White, CEO<br/>
													Coppay - Ina Samovich, CEO<br />CentBee – Heidi Patmore, Head of Marketing (TBC)</td> 
											</tr>
											<tr>
												<td style="width:150px;">12:00-12:30pm</td>
												<td>Going Meta on Bitcoin, Dr. Craig Wright (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">12:30-1:30pm</td>
												<td>Lunch</td>
											</tr>
											<tr>
												<td style="width:150px;">1:30-2:00pm</td>
												<td>Bitcoin: Global Adoption Starts Here, Kristy-Leigh Minehan (Core Scientific)</td>  
											</tr>
											<tr>
												<td style="width:150px;">2:00-2:30pm</td>
												<td>Tools of Humanity, Michael Hudson (Bitstocks)</td>
											</tr>
											<tr>
												<td style="width:150px;">2:30-3:00pm</td>
												<td>What Role Should Exchanges Play in Bitcoin Governance? MD, Circle. Former CSO, OKEx & OKCoin, Jack C. Liu (Circle)</td>
											</tr>
											<tr>
												<td style="width:150px;">3:00-3:20pm</td>
												<td>Coffee Break</td>
											</tr>
											<tr>
												<td style="width:150px;">3:30-4:30pm</td>
												<td>Growing Up: It's Time for Bitcoin and Bcommerce to Professionalize<br/>
												Moderated by: Jimmy Nguyen<br/>
												Edgar Maximillian<br/>
												Iqbal Gandham  - eToro<br/>
											Michael Hudson, TBC</td>  
											</tr>
											<!-- <tr>
												<td style="width:150px;">4:30-4:45pm</td>
												<td>Closing remarks, Jimmy Nguyen (nChain)</td>
											</tr>
											<tr>
												<td style="width:150px;">9:00pm-till late</td>
												<td>Party</td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</div>
						</div>
				
						
						
						</div>
					</article>
				</div>
			</div>
		</div>												

        <div class="row">

            <div class="col-md-6 col-lg-6 col-xl-6 text-left">


               <!--  <form id="contact-form" method="post" action="contact.php" role="form">

                    <div class="messages"></div>

					<div class="controls text-left">
                        <div class="row text-right">
                            <div class="col-12" style="font-size: 0.7rem; color: #e3e6ea;">
                                <span style="color: red; font-weight: bold">*</span> Required Field
                            </div>

                        </div>
                        <div class="row text-left align-items-start">
                            <div class="col-md-6 align-self-start">
								<div class="form-group position-relative">
									<div class="position-relative">
										<input id="form_name" type="text" name="name" class="form-control" placeholder="First Name*" required="required"
											data-error="Required!">
										<div class="help-block with-errors"></div>
									</div>
                                </div>
                            </div>
                            <div class="col-md-6 align-self-start">
								<div class="form-group">
									<div class="position-relative">
										<input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Last Name*" required="required"
											data-error="Required!">
										<div class="help-block with-errors"></div>
									</div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-start">
                                <div class="col-md-6 align-self-start">
									<div class="form-group">
										<div class="position-relative">
											<input id="form_company" type="text" name="company" class="form-control" placeholder="Company*" required="required"
											data-error="Required!">
											<div class="help-block with-errors"></div>
										</div>
                                    </div>
                                </div>
                                <div class="col-md-6 align-self-start">
									<div class="form-group">
										<div class="position-relative">
											<input id="form_position" type="text" name="position" class="form-control" placeholder="Title*" required="required"
											data-error="Required!">
											<div class="help-block with-errors"></div>
										</div>
                                    </div>
                                </div>
                            </div>
                        <div class="row align-items-start">
                            <div class="col-md-6 align-self-start">
								<div class="form-group position-relative">
										<div class="position-relative">
											<input id="form_email" type="email" name="email" class="form-control" placeholder="Email*" required="required"
												data-error="Required!">
											<div class="help-block with-errors"></div>
										</div>
                                </div>
                            </div>
                            <div class="col-md-6">
								<div class="form-group position-relative">
										<div class="position-relative">
											<input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Phone Number*" required="required"
												data-error="Required!">
											<div class="help-block with-errors"></div>
										</div>
                                </div>
                            </div>
						</div>
                        <div class="row align-items-start">
                            <div class="col-12 align-self-start">

								<div class="form-group position-relative textarea-wrap">
									<label for="form_message">Please tell us your role or work in the cryptocurrency mining industry.*</label>
										<div class="position-relative">
											<textarea id="form_message" name="message" class="form-control" placeholder="" rows="5" required="required"
												data-error="Please, leave us a message."></textarea>
											<div class="help-block with-errors"></div>
										</div>

								</div>							

                            </div>

                        </div>
                        <div class="row justify-content-center align-items-start text-center">
                            <div class="col-auto">

								<div class="form-group text-center position-relative recaptcha-wrap">
									<div class="g-recaptcha" data-sitekey="6LfN9XIUAAAAAINqkQtXdEYjSbr9Uybyok6_8U6L" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
									<input class="form-control d-none" data-recaptcha="true" required data-error="Please complete the Captcha">
									<div class="help-block with-errors"></div>
								</div>						

                            </div>

                        </div>
                        <div class="row justify-content-center align-items-start text-center">
                            <div class="col-auto">

								<input type="submit" class="btn btn-success btn-send" value="Send message">					

                            </div>

                        </div>
                        

                        <p class="text-muted">
                        </p>

                    </div>

                </form> -->

            </div>
            <!-- /.8 -->

        </div>
        <!-- /.row-->
	

	</div>
</div>

			
			<?php //include_once 'part/poweredby.php';?>
			
			
			<?php include_once '../part/footer.php'; ?>
		</div>
		<!-- JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script src="validator.js"></script>
		<script src="contact.js"></script>
		<script type="text/javascript" src="dist/js/script.min.js"></script>
		
		<script src="../dist/js/showoff.global.js" ></script>
		
		<!-- Swiper JS -->
		<script src="../dist/js/swiper.min.js"></script>
		<!-- Custom JS -->
		<script src="../dist/js/custom.js"></script>
		<!-- Parserly JS -->

		<!-- IE CSS FIX -->
		<script>
			var isIE = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
			if(isIE){
				//document.getElementsByClassName("path-rotation").style.opacity = "0";
				//var x = document.getElementsByClassName("path-rotation");
				//x[0].style.opacity = "0";
				$(".path-rotation").attr('style',  'opacity:0!important');
				//alert('internet explorer');
			}
		</script>
		<!-- Start of LiveChat (www.livechatinc.com) code -->
		<script type="text/javascript">
			window.__lc = window.__lc || {};
			window.__lc.license = 9956175;
			(function() {
				var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
				lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
			})();
		</script>
		<!-- End of LiveChat code -->
	</body>
</html>									